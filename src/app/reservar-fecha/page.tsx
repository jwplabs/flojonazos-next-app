'use client'
import React, { useContext, useEffect, useState } from 'react';
import { formatDate } from '@/utils/dateUtils';
import { Toaster } from 'sonner';
import Headers from '@/components/section/Headers';
import Footer from '@/components/section/Footer';
import ReservarFecha from '@/components/agenda/ReservarFecha';
import { useFechaContext} from '@/hooks/useFechaContext';

export default function Reserva() {
  const [fechaElegida, setFechaElegida] = useState<{ fecha: string }>({ fecha: '' });

  useEffect(() => {
    // Obtén el parámetro de fecha de la URL desde window.location
    const urlParams = new URLSearchParams(window.location.search);
    const fechaParam = urlParams.get('fecha');
    
    if (fechaParam) {
      // Ajusta la fecha según la zona horaria local del usuario
      const adjustedDate = new Date(fechaParam);
      adjustedDate.setDate(adjustedDate.getDate() + 1); // Agrega un día
      const formattedDate = formatDate(adjustedDate);
      setFechaElegida({ fecha: formattedDate });
    }
  }, []);

  const { fechaSeleccionada } = useFechaContext();

  return (
    <main className='font-puls-jakarta-sans mt-14 relative'>
    
        <Toaster richColors position="bottom-right" />
        <Headers/>
        {/*  esto caundo manda de fecha context */}
        {/*<ReservarFecha fechaElegida={{fecha:fechaSeleccionada}} />  */}
        <ReservarFecha fechaElegida={fechaElegida} />
        <Footer/>
    
    </main>
  );
}
