'use client'
import React from 'react';
import { Toaster, toast } from 'sonner'
import Headers from '@/components/section/Headers';
import Footer from '@/components/section/Footer';
import { useQrContext } from '@/hooks/useQrContext';
import QrReserva from '@/components/qr/QrReserva';

export default function Reserva() {
  const { fecha,descripcion,es_entresemana,tipo_evento, costo, url_qr } = useQrContext();
 
  return (
    <main className='font-puls-jakarta-sans  mt-14 relative' >
     <Toaster richColors position="bottom-right" />
     <Headers/>
     <QrReserva datosReserva={{fecha:fecha, descripcion:descripcion, es_entresemana:es_entresemana, tipo_evento:tipo_evento, costo:costo, url_qr:url_qr}} />
     <Footer/>
    </main>
  );
}
