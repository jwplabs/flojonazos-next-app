'use client'

import React from 'react';
import { Toaster, toast } from 'sonner'
import Headers from '@/components/section/Headers';
import Footer from '@/components/section/Footer';
import AgendaReservas from '@/components/section/AgendaReservas';

export default function Reserva() {

  return (
    <main className='font-puls-jakarta-sans  mt-14 relative' >
     <Toaster richColors position="bottom-right" />
     <Headers/>
     <AgendaReservas/>
     <Footer/>
    </main>
  );
}
