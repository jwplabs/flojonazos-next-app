import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Headers from "../components/section/Headers";
import Footer from "../components/section/Footer";
import { FechaProvider } from "@/providers/FechaContextProvider";
import { QrProvider } from "@/providers/QrContextProvider";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Flojonazos",
  description: "Fraternidad FLojonazos",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
   
    <html lang="en">
     
     
      <body className={inter.className}  >
      <FechaProvider>
      <QrProvider>
        {children}
        </QrProvider>
        </FechaProvider>
        </body>
    </html>
  );
}
