'use client'
import React, { useEffect, useState } from 'react';
import { Toaster } from 'sonner';
import CreateReserva from '@/components/agenda/CreateReserva';
import Headers from '@/components/section/Headers';
import Footer from '@/components/section/Footer';

export default function Reserva() {
  const [eventoElegida, setEventoElegida] = useState<{ tipo_evento: number }>({ tipo_evento: 0 });
  
  useEffect(() => {
    // Obtén el parámetro de id evento de la URL desde window.location
    const urlParams = new URLSearchParams(window.location.search);
    const eventoParam = urlParams.get('id');

    if (eventoParam !== null) {
      setEventoElegida({ tipo_evento: parseInt(eventoParam) });
    } else {
      // Maneja el caso donde el parámetro 'id' no está presente en la URL
    }
  }, []);

  return (
    <main className='font-puls-jakarta-sans mt-14 relative'>
      <Toaster richColors position="bottom-right" />
      <Headers />
      <CreateReserva eventoElegido={eventoElegida} />
      <Footer />
    </main>
  );
}
