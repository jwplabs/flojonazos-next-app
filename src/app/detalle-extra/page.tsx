'use client'
import React, { useEffect, useState } from 'react';
import { Toaster } from 'sonner';
import Headers from '@/components/section/Headers';
import Footer from '@/components/section/Footer';
import Deudas from '@/components/deuda/DeudasMensualidades';
import DetalleExtraordinaria from '@/components/deuda/DetalleExtraordinaria';

export default function DetalleExtra() {

  return (
    <main className='font-puls-jakarta-sans mt-14 relative'>
      <Toaster richColors position="bottom-right" />
      <Headers />
      <DetalleExtraordinaria />
      <Footer />
    </main>
  );
}
