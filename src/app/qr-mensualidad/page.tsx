'use client'
import React from 'react';
import { Toaster, toast } from 'sonner'
import Headers from '@/components/section/Headers';
import Footer from '@/components/section/Footer';
import { useQrContext } from '@/hooks/useQrContext';
import QrMensualidad from '@/components/deuda/QrMensualidad';

export default function Reserva() {
  const { fecha,descripcion,es_entresemana,tipo_evento, costo, url_qr } = useQrContext();
 
  return (
    <main className='font-puls-jakarta-sans  mt-14 relative' >
     <Toaster richColors position="bottom-right" />
     <Headers/>
     <QrMensualidad/>
     <Footer/>
    </main>
  );
}
