'use client'

import React, { useEffect, useState } from 'react';

import LoginPage from '@/components/login/LoginPage';

import { Toaster, toast } from 'sonner'

import { useRouter } from 'next/navigation';
import Spinner from '@/components/Spinner';
import { isAuthenticated } from '@/utils/auth';

export default function Home() {
  const router = useRouter();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Verifica si el usuario está autenticado al cargar la página
    if (isAuthenticated()) {
      // Si el usuario no está autenticado, redirige a la página de inicio de sesión
      router.push('/home');
    } else {
      setLoading(false); // Indica que la verificación de autenticación ha finalizado
    }
  }, [router]);

  if (loading) {
   // return <div>Loading...</div>; // Muestra un componente de carga mientras se verifica la autenticación
   return <Spinner />; // Muestra el spinner mientras se carga la página
  }
  return (
    <main className='font-puls-jakarta-sans  mt-14 relative' >
    
     <Toaster richColors position="bottom-right" />
   
     <LoginPage/>
 
      {/*Hero section start*/}
      
    </main>
  );
}
