
'use client'
import React, { useEffect, useState } from 'react';
import { useRouter } from "next/navigation"; 
import { isAuthenticated } from '@/utils/auth';
import Headers from '@/components/section/Headers';
import Spinner from '@/components/Spinner';
import Footer from '@/components/section/Footer';
import SliderImages from '@/components/section/SliderImages';
import MediaVideo from '@/components/section/MediaVideo';
import { Toaster } from 'sonner';
import AgendaReservas from '@/components/section/AgendaReservas';
import EventsCarousel from '@/components/cards/EventsCarousel';
import { refreshToken, refreshTokenIfNeeded } from '@/services/authService';
import DeudaCard from '@/components/deuda/DeudaCard';
import EventosCards from '@/components/eventos/EventosCards';
import EventosList from '@/components/eventos/EventosList';
import AgendaView from '@/components/section/AgendaView';

export default function Home() {
  const router = useRouter();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const authenticate = async () => {
      if (!isAuthenticated()) {
        localStorage.clear(); 
        router.push('/');
      } else {
        try {
          await refreshTokenIfNeeded();
        } catch (error) {
          localStorage.clear();
          router.push('/');
        }
        setLoading(false); 
      }
    };

    authenticate();
  }, [router]);

  if (loading) {
    return <Spinner />;
  }

  return (
    // poner en overflow-hidden max-w-full en main para evitar desbordamientos horizontales
    <main className='font-puls-jakarta-sans mt-14 relative overflow-hidden max-w-full'>
      <Headers/>
      <SliderImages/>
      <section id="tipoEventos">
      <EventosList/>

      </section>


      <Toaster richColors position="bottom-right" />
      <section id="agenda">
        <AgendaView />
      </section>
      <section id="deuda">
      <DeudaCard/>
      </section>
      <MediaVideo/>
      <Footer/>
    </main>
  );
}
