import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Image from "next/image";
import HoverButton from "../HoverButton";
import useButtonState from "@/hooks/useButtonState";
import { fetchEventTypes } from '@/services/eventTypeService';
import { useRouter } from "next/navigation";
import useScrollAnimation from "@/hooks/useScrollAnimation";

interface EventType {
  id: number;
  nombre: string;
  descripcion: string;
  costo_entresemana: number;
  costo_finsemana: number;
}

interface BookingItemProps {
  data: EventType; // Cambia el tipo de data a EventType
}

const BookingItem: React.FC<BookingItemProps> = ({ data }) => {
  const { handleMouseEnter } = useButtonState();
  const router = useRouter();
  
  const [selectedEventId, setSelectedEventId] = useState<number | null>(null); // Estado local para almacenar el id del evento seleccionado
  
  const handleClick = () => {
    // Al hacer clic en el botón, guardamos el id del evento en el estado local
    setSelectedEventId(data.id);
    // Redirigir a la página de reserva
    //router.push('/reserva');
    router.push(`/reserva?id=${data.id}`);
  };
  return (
    <div className="cards-events mb-10"> {/* Aplica la clase cards-events al contenedor principal */}
      <div className="card__article ml-14" style={{ marginRight: '1rem' }}> {/* Aplica la clase card__article y el estilo para el margen derecho */}
        <img className="card__img" src="https://i.postimg.cc/BbJ1r55b/card.png" alt="Card Image" width={200} height={100} />
        <div className="card__data">
          <span className="card__description ">{data.nombre}</span>
          <p>Costo entre semana: {data.costo_entresemana}</p>
          <p>Costo fin de semana: {data.costo_finsemana}</p>
          <HoverButton label="Reservar" onClick={handleClick} /> {/* Cambia el texto del botón a "BOOK NOW" */}
        </div>
      </div>
    </div>
  );
};


interface SliderSettings {
  dots: boolean;
  infinite: boolean;
  speed: number;
  slidesToScroll: number;
  slidesToShow: number;
  responsive: any;
}
const EventsCarousel: React.FC = () => {
  const [eventTypes, setEventTypes] = useState<EventType[]>([]); // Estado para almacenar los tipos de eventos

  useEffect(() => {
    const fetchData = async () => {
      try {
        const eventTypesData = await fetchEventTypes();
        setEventTypes(eventTypesData);
      } catch (error) {
        console.error('Error fetching event types:', error);
      }
    };

    fetchData();
  }, []);

  const settings: SliderSettings = {
    dots: false, // Desactiva las bolitas de paginación
    infinite: true,
    speed: 500,
    slidesToScroll: 1,
    slidesToShow: 3, // Cambia slidesToShow a 3 para mostrar tres tarjetas
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const sectionRef = useScrollAnimation();

  
    return (
      <div ref={sectionRef} className="events-carousel-container mt-12 sm:mt-24 scroll-animation" style={{ margin: '0 auto', maxWidth: 'calc(100% - 6rem)' }}> {/* Ajusta el margen izquierdo y derecho */}
        <h1 className='text-3xl sm:text-5xl font-bold mb-8 '>Tipos de Eventos</h1>
        <Slider {...settings}>
          {eventTypes.map((data, index) => (
            <BookingItem key={data.id} data={data} />
          ))}
        </Slider>
      </div>
    );
    
};

export default EventsCarousel;
