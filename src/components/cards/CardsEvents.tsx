import React, { useState, useRef, useEffect } from 'react';
import './cards.css';
import HoverButton from '../HoverButton';
import { useRouter } from 'next/navigation';
import Image from 'next/image';

interface Event {
  id: number;
  nombre: string;
  costo_entresemana: number;
  costo_finsemana: number;
}

interface Props {
  events: Event[];
}

const CardsEvents: React.FC<Props> = ({ events }) => {
  const [startIndex, setStartIndex] = useState(0);
  const containerRef = useRef<HTMLDivElement>(null);
  const [cardWidth, setCardWidth] = useState(0);
  const router = useRouter();

  useEffect(() => {
    if (containerRef.current) {
      const width = containerRef.current.getBoundingClientRect().width;
      setCardWidth(width / 3); // Configurar el ancho de la tarjeta según el tamaño del contenedor
    }
  }, []);

  const scrollRight = () => {
    if (startIndex < events.length - 3) {
      setStartIndex(startIndex + 1);
    } else if (startIndex === events.length - 3) {
      // Ajustar para que siempre muestre 3 cartas al final
      setStartIndex(events.length - 1);
    }
  };

  const scrollLeft = () => {
    if (startIndex > 0) {
      setStartIndex(startIndex - 1);
    }
  };

  const handleClick = () => {
    console.log('Botón Ver clickeado');
    router.push('/reserva');
  };

  return (
    <div className="cards-events" style={{ overflow: 'hidden', position: 'relative' }}>
   <div className="container" style={{ display: 'flex', transition: 'transform 0.5s', transform: `translateX(-${startIndex * cardWidth}px)` }} ref={containerRef}>
        {events.slice(startIndex, startIndex + 3).map((event, index) => (
          <div key={event.id} className="card__article" style={{ width: `${cardWidth}px`, marginRight: index === 2 ? 0 : '1rem' }}>
            <Image className="card__img" src="https://i.postimg.cc/BbJ1r55b/card.png" alt="Card Image" width={200} height={100} />
            <div className="card__data">
              <span className="card__description">{event.nombre}</span>
              <p>Costo entre semana: {event.costo_entresemana}</p>
              <p>Costo fin de semana: {event.costo_finsemana}</p>
              <HoverButton label="Reservar" onClick={handleClick} />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CardsEvents;
