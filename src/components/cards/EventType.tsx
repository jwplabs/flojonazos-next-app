'use client'
import React, { useState, useEffect } from 'react';
import { fetchEventTypes } from '@/services/eventTypeService';
import useScrollAnimation from '@/hooks/useScrollAnimation';
import EventsCarousel from './EventsCarousel';
import './cards.css';
const EventType = () => {
  const [eventTypes, setEventTypes] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const eventTypesData = await fetchEventTypes();
        setEventTypes(eventTypesData);
      } catch (error) {
        console.error('Error fetching event types:', error);
      }
    };

    fetchData();
  }, []);

  
 const sectionRef = useScrollAnimation();

  return (
    <div
    ref={sectionRef}
    className="bg-bg-3 p-5 sm:p-28 scroll-animation" 
  >
    <div>
    <div>
      <h1 className='text-3xl sm:text-5xl font-bold '>Tipos de Eventos</h1>
      </div>
      <EventsCarousel />
    </div>
    </div>
  );
};

export default EventType;
