import React from 'react';
import { FaCheckCircle, FaBirthdayCake, FaCircle, FaClock } from 'react-icons/fa'; // Importa los íconos de Font Awesome

export default function FloatingActionButtons() {
  return (
    <div className="cardContainer">
      <div className="card">
        <div className="floatingActionButton">
          <div className="actionItem">
            <FaCheckCircle color="red" size={40} />
            <span>Confirmado</span>
          </div>
          <div className="actionItem">
            <FaCircle color="orange" size={40} />
            <span>Cumpleaños</span>
          </div>
          <div className="actionItem">
            <FaCircle color="green" size={40} />
            <span>Pendiente</span>
          </div>
          <div className="actionItem">
            <FaClock color="purple" size={40} />
            <span>Turno</span>
          </div>
        </div>
      </div>
    </div>
  );
}
