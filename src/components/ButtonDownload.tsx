import React from 'react';

interface ButtonDownloadProps {
  url: string; // La URL de la imagen que se descargará
}

const ButtonDownload: React.FC<ButtonDownloadProps> = ({ url }) => {
  const handleClick = async () => {
    try {
      // Fetch para obtener la imagen como un blob
      const response = await fetch(url);
      const blob = await response.blob();

      // Crear un objeto URL para el blob
      const blobUrl = URL.createObjectURL(blob);

      // Crear un enlace temporal
      const link = document.createElement('a');
      link.href = blobUrl;
      link.setAttribute('download', ''); // Establecer el atributo 'download' para descargar el archivo
      document.body.appendChild(link);
      link.click(); // Simular un clic en el enlace
      document.body.removeChild(link); // Eliminar el enlace del DOM después de la descarga
    } catch (error) {
      console.error('Error al descargar la imagen:', error);
    }
  };

  return (
    <button onClick={handleClick} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
      Descargar Imagen
    </button>
  );
};

export default ButtonDownload;
