'use client'
import React, { useState } from 'react';

interface HoverButtonProps {
  label: string;
  onClick?: () => void;
}

const HoverButton: React.FC<HoverButtonProps> = ({ label, onClick }) => {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <button
      className={`bg-midred rounded-full p-2 text-white text-xs sm:px-6 sm:py-2 sm:text-base sm:m-6 hover-button ${isHovered ? 'button' : ''}`}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      onClick={onClick}
    >
      {label}
    </button>
  );
};

export default HoverButton;
