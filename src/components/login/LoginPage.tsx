'use client'
// Importa useState y useRouter
import React, { useState, forwardRef, isValidElement, cloneElement } from 'react';

import { useRouter } from "next/navigation";

// Importa los servicios de autenticación
import { login } from '@/services/authService';

// Importa los componentes necesarios de react-login-page
import { Render, Provider, Container, useStore, ContainerProps } from 'react-login-page';
import { Username } from './control/Username';
import { Password } from './control/Password';
import { Submit } from './control/Submit';
import { Logo } from './control/Logo';
import { Title } from './control/Title';
import { Banner } from './control/Banner';
import 'react-toastify/dist/ReactToastify.css';
// Importa el archivo CSS
import './index.css';
import { toast } from 'sonner';

// Define el componente RenderLogin
const RenderLogin = () => {
  const { blocks = {}, extra = {}, data } = useStore();
  const { fields, buttons } = data || { fields: [] };
  
  return (
    <Render>
      <div className="login-page2-inner">
        <aside>{blocks.banner}</aside>
        <main>
          <header>
            {blocks.logo} {blocks.title}
          </header>
          {fields
            .sort((a, b) => a.index - b.index)
            .map((item, idx) => {
              if (!item.children) return null;
              return (
                <label className={`rlp-${item.name}`} key={item.name + idx}>
                  {item.children} {extra[item.name]}
                </label>
              );
            })}
          <section>
            {buttons
              .sort((a, b) => a.index - b.index)
              .map((item, idx) => {
                const child = item.children;
                return isValidElement(child) ? cloneElement(child, { ...child.props, key: item.name + idx }) : null;
              })}
          </section>
          {blocks.buttonAfter}
        </main>
      </div>
    </Render>
  );
};

// Define el componente LoginPage
const LoginPage = forwardRef<HTMLDivElement, ContainerProps>((props, ref) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const router = useRouter();
  const handleLogin = async () => {
    try {
      const data = await login(username, password);
      // Redirecciona al usuario a la página principal
      router.push('/home');
      console.log('Login exitoso. Redireccionando...');
      toast.success('Login exitoso');
    } catch (error) {
      console.error('Error al iniciar sesión:', error);
      toast.error('Usuario o Contraseña incorrectos');
    
    }
  };
  
  
  const { children, className, ...divProps } = props;

  return (
    <Provider>
      <Banner imageUrl="/image/logo9.png"/>
      <Username value={username} onChange={(e) => setUsername(e.target.value)} />
      <Password value={password} onChange={(e) => setPassword(e.target.value)} />
      <Logo />
      <Title />

  
      <Submit onClick={handleLogin} label="Iniciar Sesión" />
      {error && <div>{error}</div>}
 
      <Container {...divProps} ref={ref} className={`login-page2 ${className || ''}`}>
        <RenderLogin />
      </Container>
      {children}
    </Provider>
  );
});

// Define el tipo de componente Login
type LoginComponent = typeof LoginPage & {
  Username: typeof Username;
  Password: typeof Password;
  Submit: typeof Submit;
  Logo: typeof Logo;
  Banner: typeof Banner;
  Title: typeof Title;
};

// Asigna los componentes a las propiedades estáticas del componente Login
const Login = LoginPage as LoginComponent;

Login.Username = Username;
Login.Password = Password;
Login.Submit = Submit;
Login.Logo = Logo;
Login.Banner = Banner;
Login.Title = Title;
// Asigna un nombre de visualización al componente LoginPage
LoginPage.displayName = 'LoginPage';

export default LoginPage;
