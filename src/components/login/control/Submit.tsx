import { FC } from 'react';
import { Button, ButtonProps } from 'react-login-page';

interface SubmitProps extends ButtonProps {
  onClick: () => void;
  label: string;
}

export const Submit: FC<SubmitProps> = ({ onClick, label, ...props }) => {
  const { keyname = 'submit', ...elmProps } = props;
  if (!elmProps.children) {
    elmProps.children = label;
  }
  return <Button type="submit" keyname={keyname} onClick={onClick} {...elmProps} />;
};

Submit.displayName = 'Login.Submit';
export default Submit;
