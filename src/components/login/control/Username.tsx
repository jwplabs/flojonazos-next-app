import { FC } from 'react';
import { Input, InputProps } from 'react-login-page';
import { UsernameIcon } from '../icons/username';

export interface UsernameProps extends InputProps {}
export const Username: FC<UsernameProps> = (props) => {
  const { keyname = 'username', name, rename, ...elmProps } = props;
  const nameBase = name || rename || keyname;
  const key = (keyname || name) as string;
  if (!elmProps.children) {
    elmProps.children = UsernameIcon;
  }
  return (
    <Input placeholder="Username" spellCheck={false} index={1} {...elmProps} type="username" name={nameBase} keyname={key} />
  );
};

Username.displayName = 'Login.Username';
export default Username;