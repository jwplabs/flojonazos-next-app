import { PropsWithChildren } from 'react';
import { Block, BlockProps, BlockTagType } from 'react-login-page';

// Extiende la interfaz BlockProps para incluir la propiedad imageUrl
interface ExtendedBlockProps<T extends BlockTagType> extends BlockProps<T> {
  imageUrl?: string;
}

export const Banner = <T extends BlockTagType = 'div'>(props: PropsWithChildren<Partial<ExtendedBlockProps<T | 'div'>>>) => {
  const { keyname = 'banner', name, imageUrl, ...elmProps } = props;
  const key = (keyname || name) as string;
  return (
    <Block {...elmProps} name={key}>
      {imageUrl && <img src={imageUrl} alt="Banner" />}
      {props.children}
    </Block>
  );
};

Banner.displayName = 'Login.Banner';
export default Banner;
