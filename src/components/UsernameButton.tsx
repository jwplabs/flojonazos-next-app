// Importa el hook useRouter de Next.js
import React, { useState } from 'react';
import { useRouter } from "next/navigation";

// Define la interfaz para las props del UsernameButton
interface UsernameButtonProps {
  label: string;
  destination: string; // Prop para especificar el destino de la navegación
}

// Define el componente UsernameButton
const UsernameButton: React.FC<UsernameButtonProps> = ({ label, destination }) => {
  const [isHovered, setIsHovered] = useState(false);
  const router = useRouter(); // Obtiene el router

  // Función para manejar el clic en el botón y navegar a la página de destino
  const handleClick = () => {
    router.push(destination); // Navega a la página de destino cuando se hace clic
  };

  return (
    <button
      className={`text-lg menu-item ${isHovered ? 'active' : ''}`} // Clases CSS condicionales
      onMouseEnter={() => setIsHovered(true)} // Maneja el evento onMouseEnter
      onMouseLeave={() => setIsHovered(false)} // Maneja el evento onMouseLeave
      onClick={handleClick} // Maneja el evento onClick
    >
      {label} {/* Muestra la etiqueta del botón */}
    </button>
  );
};

export default UsernameButton;
