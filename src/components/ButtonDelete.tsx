import React, { FC, useState } from 'react';

interface ButtonDeleteProps {
  buttonText?: string;
  onClick?: () => void;
  href?: string; // Nueva propiedad para la URL
}

export const ButtonDelete: FC<ButtonDeleteProps> = ({ buttonText = 'Eliminar', onClick, href }) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleClick = () => {
    if (href) {
      window.location.href = href; // Redirige a la URL si está definida
    } else if (onClick) {
      onClick(); // Ejecuta la función onClick si está definida
    }
  };

  return (
    <button
      className={`bg-midred hover:bg-red-600 text-white font-semibold px-2 py-1 rounded responsive-button lg:inline-block lg:mr-0 hover-delete ${isHovered ? 'button' : ''}`}
      onClick={handleClick} // Usamos handleClick en lugar de onClick directamente
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {buttonText}
    </button>
  );
};
