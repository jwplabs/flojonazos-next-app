import React from 'react';
import Image from 'next/image';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import useCombinedScrollAnimations from '@/hooks/useCombinedScrollAnimation';

const EventsEjemplo = () => {
  const cards = [
    {
      article: "Article",
      date: "on July 25, 2023",
      heading: "Animating CSS Grid (How To + Examples)",
      genres: ["Animation", "react", "grid", "grid-animation", "system-log", "Animation-log"],
      publisher: "nawfal imran",
    },
    {
      article: "Article2",
      date: "on July 25, 2023",
      heading: "Animating CSS Grid (How To + Examples)2",
      genres: ["Animation", "react", "grid", "grid-animation", "system-log", "Animation-log"],
      publisher: "nawfal imran2",
    },
    {
      article: "Article3",
      date: "on July 25, 2023",
      heading: "Animating CSS Grid (How To + Examples)3",
      genres: ["Animation", "react", "grid", "grid-animation", "system-log", "Animation-log"],
      publisher: "nawfal imran3",
    },
    {
      article: "Article4",
      date: "on July 25, 2023",
      heading: "Animating CSS Grid (How To + Examples)4",
      genres: ["Animation", "react", "grid", "grid-animation", "system-log", "Animation-log"],
      publisher: "nawfal imran4",
    },
    {
      article: "Article5",
      date: "on July 25, 2023",
      heading: "Animating CSS Grid (How To + Examples)5",
      genres: ["Animation", "react", "grid", "grid-animation", "system-log", "Animation-log"],
      publisher: "nawfal imran5",
    },
    // Add more card data as needed
  ];

  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 4
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return (
    
    <div className="backgroundColorEventos">
      <Carousel
        responsive={responsive}
        ssr
        infinite
        autoPlay
        autoPlaySpeed={3000}
        keyBoardControl
        customTransition="all .5"
        transitionDuration={500}
        containerClass="carousel-container"
        removeArrowOnDeviceType={["tablet", "mobile"]}
        dotListClass="custom-dot-list-style"
        itemClass="carousel-item-padding-40-px"
        showDots
      >
        {cards.map((card, index) => (
          <div key={index} className="cardListItemEventos">
            <div className="cardItemContentEventos">
              <div>
                <div className="cardItemContentHeaderEventos">
                  <p className="cardItemContentHeaderArticleEventos">{card.article}</p>
                  <p className="cardItemContentHeaderDateEventos">&#160;{card.date}</p>
                </div>
                <h1 className="cardItemContentHeadingEventos">{card.heading}</h1>
                <div className="cardItemContentGenreSectionEventos">
                  {card.genres.map((genre, genreIndex) => (
                    <p key={genreIndex} className="cardItemContentGenreItemEventos">{genre}</p>
                  ))}
                </div>
              </div>
              <div className="cardItemContentPublisherSectionEventos">
                <Image src="/path-to-your-image.jpg" alt="Publisher Image" width={50} height={50} />
                <h1>{card.publisher}</h1>
              </div>
            </div>
          </div>
        ))}
      </Carousel>
    </div>
   
  );
};

export default EventsEjemplo;
