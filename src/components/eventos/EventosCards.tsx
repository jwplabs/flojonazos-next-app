import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import useCombinedScrollAnimations from '@/hooks/useCombinedScrollAnimation';
import { fetchEventTypes } from '@/services/eventTypeService';
import EventType from '@/models/eventTypeModel';
import HoverButton from '../HoverButton';
import { useRouter } from 'next/navigation';

const EventosCards = () => {
  const [eventTypes, setEventTypes] = useState<EventType[]>([]); // Estado para almacenar los tipos de eventos
  // Estado para almacenar los tipos de eventos
  const { sectionRef, leftSectionRef, rightSectionRef } = useCombinedScrollAnimations();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const eventTypesData = await fetchEventTypes();
        setEventTypes(eventTypesData);
      } catch (error) {
        console.error('Error fetching event types:', error);
      }
    };

    fetchData();
  }, []);

  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 4
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };
  const router = useRouter();

  const [selectedEventId, setSelectedEventId] = useState<number | null>(null); // Estado local para almacenar el id del evento seleccionado


  const handleClick = (id: number) => {
    // Al hacer clic en el botón, guardamos el id del evento en el estado local
    setSelectedEventId(id);
    // Redirigir a la página de reserva
    //router.push('/reserva');
    router.push(`/reserva?id=${id}`);
  };

  return (
    <div className="backgroundColorEventos" ref={sectionRef}>
      <Carousel
        responsive={responsive}
        ssr
        infinite
        autoPlay
        autoPlaySpeed={3000}
        keyBoardControl
        customTransition="all .5"
        transitionDuration={500}
        containerClass="carousel-container"
        removeArrowOnDeviceType={["tablet", "mobile"]}
        dotListClass="custom-dot-list-style"
        itemClass="carousel-item-padding-40-px"
        showDots
      >
        {eventTypes.map((eventType, index) => (
          <div key={index} className="cardListItemEventos">
            <div className="cardItemContentEventos">
              <div>
                <div className="cardItemContentHeaderEventos">
                  <p className="cardItemContentHeaderArticleEventos">{eventType.nombre}</p>
                  <p className="cardItemContentHeaderDateEventos">&#160;</p> {/* No hay fecha en los datos de evento */}
                </div>
                <h1 className="cardItemContentHeadingEventos">
                  {eventType.descripcion.length > 150
                    ? `${eventType.descripcion.substring(0, 150)}...`
                    : eventType.descripcion}
                </h1>

                <div className="cardItemContentGenreSectionEventos">
                  <p className="cardItemContentGenreItemEventos">Costo entre semana: {eventType.costo_entresemana}</p>
                  <p className="cardItemContentGenreItemEventos">Costo fin de semana: {eventType.costo_finsemana}</p>
                </div>
              </div>
              <div className="cardItemContentPublisherSectionEventos">
                {/* Puedes dejar este espacio en blanco si no tienes una imagen */}
                {/* <Image src="/path-to-your-image.jpg" alt="Publisher Image" width={50} height={50} /> */}
                <HoverButton label="Reservar" onClick={() => handleClick(eventType.id)} />

              </div>
            </div>
          </div>
        ))}
      </Carousel>
    </div>
  );
};

export default EventosCards;
