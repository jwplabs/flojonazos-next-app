
import React, { useEffect, useState } from "react";
import { Card, CardHeader, CardFooter, Image, Button } from "@nextui-org/react";
import useCombinedScrollAnimations from "@/hooks/useCombinedScrollAnimation";
import HoverButton from "../HoverButton";
import { useRouter } from "next/navigation";
import { fetchDeudasByMonth } from "@/services/deudaService";
import DeudaExtraordinariaModel, { Extraordinaria } from '@/models/deudaExModel';
import { fetchCurrentDeudaExtraordinaria } from "@/services/deudaExService";
import EventosCards from "./EventosCards";


export default function EventosList() {
   //const sectionRef = useScrollAnimation();

   const [currentDeuda, setCurrentDeuda] = useState(new DeudaExtraordinariaModel(new Extraordinaria(0, 0, '', ''), 0, 0));

   const [totalDeuda, setTotalDeuda] = useState(0);
   const { sectionRef, leftSectionRef, rightSectionRef } = useCombinedScrollAnimations();
   const router = useRouter();
 
  return (
    <div ref={sectionRef} className=" calendarContainerFloat scroll-animation flex justify-center items-center min-h-screen mt-10">
     
     <h1 className="text-3xl sm:text-6xl font-bold mb-14 md:col-span-2">Tipos de Eventos</h1>
          <EventosCards/>
        </div>
   
  );
}
