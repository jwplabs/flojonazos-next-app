import React, { useState } from 'react';
import { useRouter } from "next/navigation";

interface NavigationButtonProps {
  label: string;
  destination: string; // Nueva prop para especificar el destino de la navegación
}

const NavigationButton: React.FC<NavigationButtonProps> = ({ label, destination }) => {
  const [isHovered, setIsHovered] = useState(false);
  const router = useRouter(); // Obtén el router

  // Función para manejar el clic en el botón y navegar a la página de destino
  const handleClick = () => {
    router.push(destination);
  };

  return (
    <button
    className={`w-24 h-8 bg-midgreen text-white rounded-full text-lg menu-item ${isHovered ? 'button' : ''}`}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      onClick={handleClick} 
    >
      {label}
    </button>
  );
};

export default NavigationButton;
