import React from 'react';
import { ButtonAceptar } from '../ButtonAceptar';
import Image from 'next/image';
import ButtonDownload from '../ButtonDownload';
import { createAgenda } from '@/services/agendaService';
import { toast } from 'sonner';
import { useRouter } from 'next/navigation';
import { sendEmail } from '@/utils/sendEmailUtils';

interface DatosReserva {
  fecha: string;
  descripcion: string;
  es_entresemana: boolean;
  tipo_evento: number;
  costo: string;
  url_qr: string;
}

const QrReserva: React.FC<{ datosReserva: DatosReserva }> = ({ datosReserva }) => {
  const router = useRouter();

  const handleClickAceptar = async () => {
    const user_id = localStorage.getItem('user_id') ?? 'noencontrado';

    try {
      const agenda = await createAgenda({
        fecha: datosReserva.fecha,
        horaInicio: '11:57:00',
        horaFin: '11:57:00',
        descripcion: datosReserva.descripcion,
        tipoEvento: datosReserva.tipo_evento,
        esEntreSemana: datosReserva.es_entresemana,
        estadoReserva: 'pendiente',
        user: user_id,
      });

      console.log('Agenda creada:', agenda);
      toast('Reserva Agendada');
      router.push('/');
    } catch (error) {
      console.error('Error al crear la agenda:', error);
      toast('Datos Incorrectos');
    }
  };

  // Renderiza el botón solo si todos los campos requeridos de datosReserva tienen valores válidos
  const renderizarBotonAceptar = () => {
    const { fecha, descripcion, costo, url_qr } = datosReserva;
    if (fecha && descripcion && costo && url_qr) {
      return (
        <ButtonAceptar buttonText="Aceptar" onClick={handleClickAceptar} />
      );
    }
    return null;
  };

  return (
    <div className="flex">
      <div className="flex-grow bg-gray-100">
        <div className="container mx-auto p-4 lg:mt-0 lg:pl-76">
          <div className="bg-white rounded shadow p-4">
            <h1 className="text-2xl font-semibold mb-4">Solicitar Reserva</h1>
            <div className="mb-4">
              <label htmlFor="descripcion" className="text-xs required">
                Observación (Nombre del pagante): {datosReserva.descripcion}
              </label>
            </div>
            <div className="mb-4">
              <label htmlFor="costo" className="text-xs required">
                Costo: {datosReserva.costo}
              </label>
            </div>
            <div className="mb-4">
              <label htmlFor="fecha" className="text-xs required">
                Fecha: {datosReserva.fecha}
              </label>
            </div>
            <img className="card__img" src={datosReserva.url_qr} alt="Card Image" width={350} height={350} />
            <ButtonDownload url={datosReserva.url_qr} />
            <div className="footer flex justify-end">
              {/* Renderiza el botón de aceptar condicionalmente */}
              {renderizarBotonAceptar()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default QrReserva;
