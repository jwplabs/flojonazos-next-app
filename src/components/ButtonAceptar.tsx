import React, { FC, useState } from 'react';

interface ButtonAceptarProps {
  buttonText?: string;
  onClick?: () => void;
}

export const ButtonAceptar: FC<ButtonAceptarProps> = ({ buttonText = 'Guardar', onClick }) => {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <button
      className={`bg-midred rounded-full p-2 text-white text-xs sm:px-6 sm:py-2 sm:text-base sm:m-6 hover:bg-indigo-600 hover-button ${isHovered ? 'button' : ''}`}
      onClick={onClick}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {buttonText}
    </button>
  );
};
