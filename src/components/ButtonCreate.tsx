import React, { FC, useState } from 'react';

interface ButtonCreateProps {
  buttonText?: string;
  onClick?: () => void;
}

export const ButtonCreate: FC<ButtonCreateProps> = ({ buttonText = 'Guardar', onClick }) => {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <button
      className={`bg-midamber2 rounded-full p-2 text-white text-xs sm:px-6 sm:py-2 sm:text-base sm:m-6 hover:bg-indigo-600 hover-create ${isHovered ? 'button' : ''}`}
      onClick={onClick}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {buttonText}
    </button>
  );
};
