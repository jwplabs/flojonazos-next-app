import React, { FC, useState } from 'react';

interface ButtonEditProps {
  buttonText?: string;
  onClick?: () => void;
  href?: string; // Nueva propiedad para la URL
}

export const ButtonEdit: FC<ButtonEditProps> = ({ buttonText = 'Guardar', onClick, href }) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleClick = () => {
    if (href) {
      window.location.href = href; // Redirige a la URL si está definida
    } else if (onClick) {
      onClick(); // Ejecuta la función onClick si está definida
    }
  };

  return (
    <button
      className={`bg-midblue hover:bg-blue-600 text-white font-semibold px-2 py-1 rounded responsive-button lg:inline-block lg:mr-2 mb-2 hover-edit ${isHovered ? 'button' : ''}`}
      onClick={handleClick} // Usamos handleClick en lugar de onClick directamente
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {buttonText}
    </button>
  );
};
