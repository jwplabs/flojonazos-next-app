import React, { useState } from 'react';
import { FaMoneyBillTrendUp } from 'react-icons/fa6';
import { HiMenu, HiOutlineMail, HiOutlineRefresh, HiSave, HiUser, HiUsers } from 'react-icons/hi'; 
import { MdExitToApp } from 'react-icons/md';
import { TfiAgenda } from 'react-icons/tfi';

const Sidebar: React.FC = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className={`sidebar ${isOpen ? 'open' : ''}`}>
      <ul>
        <li>
          <div className="sidebar-menu" onClick={toggleMenu}>
            <HiMenu size={20} />
          </div>
        </li>
        <li>
          <div className="sidebar-menu">
            {isOpen ? (
              <>
                <HiUser size={20} />
                <span className="menu-text">Inicio</span>
              </>
            ) : (
              <HiUser size={20} />
            )}
          </div>
        </li>
        <li>
          <div className="sidebar-menu" >
            {isOpen ? (
              <>
                <TfiAgenda size={20} />
                <span className="menu-text">Agenda</span>
              </>
            ) : (
              <TfiAgenda size={20} />
            )}
          </div>
        </li>
        <li>
          <div className="sidebar-menu">
            {isOpen ? (
              <>
                <FaMoneyBillTrendUp size={20} />
                <span className="menu-text">Pagos</span>
              </>
            ) : (
              <FaMoneyBillTrendUp size={20} />
            )}
          </div>
        </li>
        <li>
          <div className="sidebar-menu">
            {isOpen ? (
              <>
                <HiOutlineRefresh size={20} />
                <span className="menu-text">Turnos</span>
              </>
            ) : (
              <HiOutlineRefresh size={20} />
            )}
          </div>
        </li>
        <li>
          <div className="sidebar-menu">
            {isOpen ? (
              <>
                <HiUsers size={20} />
                <span className="menu-text">Grupos</span>
              </>
            ) : (
              <HiUsers size={20} />
            )}
          </div>
        </li>
  
        <li>
          <div className="sidebar-menu">
            {isOpen ? (
              <>
                <MdExitToApp size={20} />
                <span className="menu-text">Salir</span>
              </>
            ) : (
              <MdExitToApp size={20} />
            )}
          </div>
        </li>
      </ul>
      
    </div>
  );
};

export default Sidebar;
