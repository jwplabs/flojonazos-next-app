'use client'
import { useEffect, useRef, useState } from 'react';
import Datepicker from 'tailwind-datepicker-react';
import { CheckIcon, ChevronDownIcon } from '@heroicons/react/20/solid';


interface DatePickerProps {
  setFechaElegida: (fecha: string) => void; // Define el tipo de la prop setFechaElegida
}
const options = {
  autoHide: true,
  todayBtn: true,
  clearBtn: true,
  clearBtnText: 'Clear',
  maxDate: new Date('2030-01-01'),
  minDate: new Date(),
  theme: {
    background: "bg-[#3bb24d]",
    todayBtn: "bg-[#3bb24d]",
    clearBtn: "bg-[#3bb24d]",
    icons: "bg-[#3bb24d]",
    text: "bg-[#3bb24d]",
    disabledText: "bg-red-500",
    input: "bg-[#3bb24d]",
    inputIcon: "bg-[#3bb24d]",
    selected: "bg-[#34CAC6]",
  },
  icons: {
    prev: () => <span>Ant.</span>,
    next: () => <span>Sig.</span>,
  },
  datepickerClassNames: 'top-18',
  defaultDate: new Date(),
  language: 'es',
  disabledDates: [],
  weekDays: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
  inputNameProp: 'date',
  inputIdProp: 'date',
  inputPlaceholderProp: 'Seleccionar fecha',
  //   inputDateFormatProp: {
  //     day: 'numeric',
  //     month: 'long',
  //     year: 'numeric',
  //   },
};

export default function DatePicker({ setFechaElegida }: DatePickerProps) {
  const [show, setShow] = useState(false);
  const [selectedDate, setSelectedDate] = useState<Date | null>(null)

  const datepickerRef = useRef<HTMLDivElement>(null);

  // Dentro de la función handleChange
const handleChange = (selectedDate: Date) => {
  const formattedDate = selectedDate.toISOString().split('T')[0]; // Formatea la fecha en YYYY-MM-DD
  setSelectedDate(selectedDate);
  setFechaElegida(formattedDate); // Guarda la fecha formateada
  console.log(formattedDate);
};



  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (datepickerRef.current && !datepickerRef.current.contains(event.target as Node)) {
        setShow(false);
      }
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  const handleClose = (state: boolean) => {
    setShow(state);
  };
  
  return (
    <div>
      <Datepicker options={options} onChange={handleChange} show={show} setShow={setShow}>
        <div className="relative mb-4">
          <input
            type="text"
            className="w-full p-2 flex-grow border bg-[#3bb24d] text-white rounded-lg"
            placeholder=" "
            value={selectedDate?.toLocaleDateString() || ''}
            onFocus={() => setShow(true)}
            readOnly
          />
          <div className="absolute inset-y-0 right-2 flex items-center">
            <ChevronDownIcon
              className="h-7 w-7 fill-white bg-[#3bb24d] rounded cursor-pointer"
              aria-hidden="true"
              onClick={() => setShow(!show)}
            />
          </div>

        </div>
      </Datepicker>
    </div>

  )
}
