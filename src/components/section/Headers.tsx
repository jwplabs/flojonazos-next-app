'use client'
import React from 'react';
import useMenuState from '@/hooks/useMenuState';

import UsernameButton from '../UsernameButton';
import CerrarSesionButton from './CerrarSesionButton';
import { useRouter } from 'next/navigation';
import Image from 'next/image';

const Headers = () => {
  const {
    menuOpen,
    toggleMenu,
    hoveredItem,
    handleMouseEnter,
    handleMouseLeave,
  } = useMenuState();

  const router = useRouter();
  const username = typeof window !== 'undefined' ? localStorage.getItem('username') : null;
/*
const handleClick = () => {

  router.push('/home');
};
const handleClickAgendaPage = () => {
  router.push('/agenda');
};

const handleClickAgenda = () => {
  router.push('/home#agenda');
};
const handleClickEvento = () => {
  router.push('/home#tipoEventos');
};
const handleClickDeuda = () => {
  router.push('/home#deuda');
}; */
const handleClick = () => {

  router.push('/home');
  
};
const handleClickAgenda = () => {
  //router.push('/home');
  router.push('/home#agenda');
  setTimeout(() => { // Espera un momento para asegurarse de que la navegación se haya completado
   // window.location.hash = 'agenda';  // Esto cambiará la URL a /home#deuda
    const element = document.getElementById('agenda');
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }, 100); // Ajusta el tiempo según sea necesario
};
const handleClickEvento = () => {
  //router.push('/home');
  router.push('/home#tipoEventos');
  setTimeout(() => { // Espera un momento para asegurarse de que la navegación se haya completado
   // window.location.hash = 'tipoEventos';  // Esto cambiará la URL a /home#deuda
    const element = document.getElementById('tipoEventos');
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }, 100); // Ajusta el tiempo según sea necesario
};

const handleClickDeuda = () => {
  // Navega a la página principal, sin hash
  router.push('/home#deuda'); 

  // Luego, una vez la navegación ha terminado, actualiza el hash directamente
  setTimeout(() => {
    //window.location.hash = 'deuda';  // Esto cambiará la URL a /home#deuda
    const element = document.getElementById('deuda');  // Asegúrate de que el elemento exista
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });  // Desplazamiento suave
    }
  }, 100);  // Ajusta el tiempo si es necesario
};




return (
  <header className="fixed top-0 w-full bg-white z-10 sm:overflow-hidden font-puls-jakarta-sans">
    <div className="flex justify-between items-center p-6 xl:mx-16">
      {/* Logo */}
      
      <div onClick={handleClick} style={{ cursor: 'pointer' }}>
      <img src="/image/logo2.png" alt="Logo" width={150} height={150} />
    </div>

      {/* Menú para pantallas grandes */}
      <ul className={`flex gap-6 ${menuOpen ? 'hidden' : 'md:flex'} sm:text-lg`}>
        <li
          className={`hidden md:inline text-slate-700 menu-item ${hoveredItem === 'home' ? 'active' : ''}`}
          onMouseEnter={() => handleMouseEnter('home')}
          onMouseLeave={handleMouseLeave}
          onClick={handleClick}
        >
          
          home
        </li>
        <li
          className={`hidden md:inline text-slate-700 menu-item ${hoveredItem === 'evento' ? 'active' : ''}`}
          onMouseEnter={() => handleMouseEnter('evento')}
          onMouseLeave={handleMouseLeave}
          onClick={handleClickEvento}
        >
          eventos
        </li>

        <li
          className={`hidden md:inline text-slate-700 menu-item ${hoveredItem === 'agenda' ? 'active' : ''}`}
          onMouseEnter={() => handleMouseEnter('agenda')}
          onMouseLeave={handleMouseLeave}
          onClick={handleClickAgenda}
        >
          agenda
        </li>
        <li
          className={`hidden md:inline text-slate-700 menu-item ${hoveredItem === 'deuda' ? 'active' : ''}`}
          onMouseEnter={() => handleMouseEnter('deuda')}
          onMouseLeave={handleMouseLeave}
          onClick={handleClickDeuda}
        >
          deudas
        </li>
      
      </ul>

      <div className='flex gap-4 mx-0 md:mr-12'>
        
        <UsernameButton label={`${username || 'Guest'}`} destination="/Perfil" />
        <CerrarSesionButton label="Cerrar" destination="/" />
      </div>
      <div className="md:hidden">
        <button onClick={toggleMenu} className="text-xl">
          ☰
        </button>
      </div>

      {/* Menú desplegable para pantallas pequeñas */}
      {menuOpen && (
        <div className="md:hidden absolute top-16 left-0 w-full bg-white p-4 overflow-hidden max-w-full">
          <ul className="flex flex-col gap-2">
            <li  onClick={handleClick} className="text-slate-700">home</li>
            <li  onClick={handleClickEvento} className="text-slate-700">eventos</li>
            <li  onClick={handleClickAgenda} className="text-slate-700">agenda</li>
            <li  onClick={handleClickDeuda} className="text-slate-700">deudas</li>
    
          </ul>
        </div>
      )}
    </div>
  </header>
);
}

export default Headers;
