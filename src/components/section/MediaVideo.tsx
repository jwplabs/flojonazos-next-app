import React, { useRef, useState, useEffect } from 'react';
import useScrollAnimation from '@/hooks/useScrollAnimation';
import { fetchMediaVideos } from '@/services/videoService';

const MediaVideo = () => {
  const playerRef = useRef<any>(null);
  const sectionRef = useScrollAnimation();
  const [lastVideo, setLastVideo] = useState<any>(null);

  useEffect(() => {
    const fetchLastVideo = async () => {
      try {
        const videos = await fetchMediaVideos();
        if (videos && videos.length > 0) {
          const latestVideo = videos[videos.length - 1]; // Obtener el último video de la lista
          setLastVideo(latestVideo);
        }
      } catch (error) {
        console.error('Error fetching last video:', error);
      }
    };

    fetchLastVideo();
  }, []);

  // Funcion para extraer el ID del video de la URL de YouTube
  const getYoutubeVideoId = (url: string) => {
    let videoId = '';
  
    // Si la URL es del formato https://www.youtube.com/embed/t8_huMOj1_0
    const embedRegex = /(?:embed\/|v=)([^&?/]+)/;
    const embedMatch = url.match(embedRegex);
    if (embedMatch && embedMatch.length > 1) {
      videoId = embedMatch[1];
    }
  
    // Si la URL es del formato https://youtu.be/Pm4i1URWnVQ?si=mWWWC288OKrCSQD4
    if (!videoId) {
      const shortUrlRegex = /(?:youtu\.be\/|v=)([^&?/]+)/;
      const shortUrlMatch = url.match(shortUrlRegex);
      if (shortUrlMatch && shortUrlMatch.length > 1) {
        videoId = shortUrlMatch[1];
      }
    }
  
    return videoId;
  };

  return (
    <div ref={sectionRef} className="p-3 sm:p-16 xl:p-[128px] scroll-animation">
      <div>
      {lastVideo && (
        <h1 className="text-3xl sm:text-6xl font-bold text-center">{lastVideo.titulo}</h1>
      )}
        </div>

      <div className="mt-6">
        {lastVideo && (
          <div className="bg-bg-3 p-6 rounded-xl flex items-center justify-center">
            <iframe
              ref={playerRef}
              width="560"
              height="315"
              src={`https://www.youtube.com/embed/${getYoutubeVideoId(lastVideo.url)}`}
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            ></iframe>
          </div>
        )}
      </div>
    </div>
  );
};

export default MediaVideo;
