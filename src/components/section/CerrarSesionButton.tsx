import React, { useState } from 'react';
import { useRouter } from "next/navigation";

interface CerrarSesionButtonProps {
  label: string;
  destination: string; // Nueva prop para especificar el destino de la navegación
}

const CerrarSesionButton: React.FC<CerrarSesionButtonProps> = ({ label, destination }) => {
  const [isHovered, setIsHovered] = useState(false);
  const router = useRouter(); // Obtén el router

  // Función para manejar el clic en el botón y navegar a la página de destino
  const handleClick = () => {
    // Verifica si localStorage está definido (solo disponible en el cliente)
  if (typeof window !== 'undefined' && window.localStorage) {
    // Limpiar datos del almacenamiento local
    localStorage.clear();
  }
  
    // Redirigir al usuario a la página de destino
    router.push(destination);
  };

  return (
    <button
      className={`w-24 h-8 bg-midgreen text-white rounded-full text-lg menu-item ${isHovered ? 'button' : ''}`}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      onClick={handleClick} // Usa la función handleClick para manejar el clic
    >
      {label}
    </button>
  );
};

export default CerrarSesionButton;
