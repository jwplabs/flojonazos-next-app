import React, { useEffect, useState } from 'react';
import "../../app/globals.css";
import { fetchMediaImages } from '../../services/mediaImageService';
import MediaImages from '@/models/mediaImageModel';

const SliderImages = () => {
  const [mediaImages, setMediaImages] = useState<MediaImages[]>([]);

  useEffect(() => {
    const loadMediaImages = async () => {
      try {
        const images = await fetchMediaImages();
        setMediaImages(images);
      } catch (error) {
        console.error('Error fetching media images:', error);
      }
    };

    loadMediaImages();
  }, []);

  return (
<div className="m-7 mb-48">
<div className="bg-white m-2 p-12 flex flex-col gap-8 overflow-hidden max-w-full">
        <div
          className="box-slider"
          style={{
            position: 'relative',
            width: '200px',
            height: '200px',
            transformStyle: 'preserve-3d',
            animation: 'animate 20s linear infinite',
            margin: '100px auto', // Centrar horizontalmente en la vista
          }}
        >
          {mediaImages.map((image, index) => (
            <span
              key={image.id}
              style={{
                position: 'absolute',
                top: '0',
                left: '0',
                width: '100%',
                height: '100%',
                transformOrigin: 'center',
                transformStyle: 'preserve-3d',
                transform: `rotateY(calc(${index} * ${360 / mediaImages.length}deg)) translateZ(400px)`,
                WebkitBoxReflect: 'below 0px linear-gradient(transparent,transparent, #0004)',
              }}
            >
              <img
                src={image.url}
                alt={image.descripcion}
                style={{
                  position: 'absolute',
                  top: '0',
                  left: '0',
                  width: '100%',
                  height: '100%',
                  objectFit: 'cover',
                }}
              />
            </span>
          ))}
        </div>
      </div>
    </div>
  );
};

export default SliderImages;
