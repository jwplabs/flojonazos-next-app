
import React, { useState, useEffect, useContext } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { fetchBirthdaysDisponible } from '@/services/birthdayService';
import { fetchTurnos } from '@/services/turnoService';
import { fetchAgendasPendientes, fetchAgendasConfirmadas } from '@/services/agendaService';

import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'sonner';
import { fetchUsers } from '@/services/usuarioService';
import { fetchGroups } from '@/services/groupService';
import useScrollAnimation from '@/hooks/useScrollAnimation';
import Agenda from '@/models/agendaModel';
import Birthday from '@/models/birthdayModel';
import Turno from '@/models/turnoModel';
import User from '@/models/usuarioModel';
import Group from '@/models/groupModel';
import { FaCheckCircle, FaCircle, FaClock } from 'react-icons/fa';
import useScrollAnimationLeft from '@/hooks/useScrollAnimationLeft';
import useCombinedScrollAnimations from '@/hooks/useCombinedScrollAnimation';
import { useRouter } from 'next/navigation';
import { formatDate } from '@/utils/dateUtils';
import { useFechaContext } from '@/hooks/useFechaContext';
//import { FechaProvider, useFechaContext } from '@/hooks/useFechaContextJunto';

const AgendaReservas: React.FC = () => {
  const [agendasPendientes, setAgendasPendientes] = useState<Agenda[]>([]);
  const [agendasConfirmadas, setAgendasConfirmadas] = useState<Agenda[]>([]);
  const [date, setDate] = useState<Date | null>(new Date());

  const [birthdays, setBirthdays] = useState<Birthday[]>([]);
  const [turnos, setTurnos] = useState<Turno[]>([]);
  const [users, setUsers] = useState<User[]>([]);
  const [groups, setGroups] = useState<Group[]>([]);
  const router = useRouter();


  //mandar dato
  // Mandar dato
  const { setFechaSeleccionada } = useFechaContext();

  useEffect(() => {
    const fetchData = async () => {
      const birthdaysData = await fetchBirthdaysDisponible();
      setBirthdays(birthdaysData);
    };
    fetchData();
  }, []);

  useEffect(() => {
    const fetchTurnosData = async () => {
      try {
        const turnosData = await fetchTurnos();
        setTurnos(turnosData);
      } catch (error) {
        console.error('Error fetching turnos:', error);
      }
    };
    fetchTurnosData();
  }, []);

  useEffect(() => {
    const fetchUsersData = async () => {
      try {
        const usersData = await fetchUsers();
        setUsers(usersData);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };
    fetchUsersData();
  }, []);

  useEffect(() => {
    const fetchGroupsData = async () => {
      try {
        const groupsData = await fetchGroups();
        setGroups(groupsData);
      } catch (error) {
        console.error('Error fetching groups:', error);
      }
    };
    fetchGroupsData();
  }, []);

  useEffect(() => {
    const fetchAgendas = async () => {
      try {
        const pendientes = await fetchAgendasPendientes();
        const confirmadas = await fetchAgendasConfirmadas();
        setAgendasPendientes(pendientes);
        setAgendasConfirmadas(confirmadas);
      } catch (error) {
        console.error('Error fetching agendas:', error);
      }
    };
    fetchAgendas();
  }, []);

  const handleClickDay = (date: Date) => {
    console.log('Fecha seleccionada:', date);
    console.log('Lista de cumpleaños:', birthdays);

    const selectedDate = new Date(date);

    // Comparando con turnos
    const selectedDateString = `${selectedDate.getUTCFullYear()}-${selectedDate.getUTCMonth() + 1}-${selectedDate.getUTCDate()}`;

    const turno = turnos.find(turno => {
      const turnoDate = new Date(turno.fecha);
      const turnoDateString = `${turnoDate.getUTCFullYear()}-${turnoDate.getUTCMonth() + 1}-${turnoDate.getUTCDate()}`;
      return turnoDateString === selectedDateString;
    });

    if (turno) {
      const group = groups.find(group => group.id === turno.grupo_turno);
      if (group) {
        console.log('Grupo del turno:', group.nombre);
        toast('Grupo del turno: ' + group.nombre);
      }
    }


    // Comparando con cumpleaños
    const birthday = birthdays.find(birthday => {
      const birthdayDate = new Date(birthday.fecha);
      return (
        birthdayDate.getDate() === selectedDate.getDate() &&
        birthdayDate.getMonth() === selectedDate.getMonth()
      );
    });

    if (birthday) {
      const user = users.find(user => user.id === birthday.user);
      if (user) {
        console.log('Usuario del cumpleaños:', user.full_name);
        toast('Cumpleañero del dia: ' + user.full_name);
      }
    }

    const agendaPendiente = agendasPendientes.find(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === selectedDateString;
    });

    if (agendaPendiente) {
      console.log('Agenda pendiente encontrada:');
      // Aquí puedes manejar la lógica para mostrar la agenda pendiente según tus requerimientos
      toast('Reservado: ' + agendaPendiente.descripcion);
    }

    // Comparando con agendas confirmadas


    const agendaConfirmada = agendasConfirmadas.find(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === selectedDateString;
    });

    if (agendaConfirmada) {
      console.log('Agenda pendiente encontrada:');
      // Aquí puedes manejar la lógica para mostrar la agenda pendiente según tus requerimientos
      toast('Reservado: ' + agendaConfirmada.descripcion);
    }

    if (!turno && !birthday && !agendaPendiente && !agendaConfirmada) {
      // La fecha está libre, redirige al usuario



      // Guarda la fecha en el contexto
      //router.push('/reservar-fecha');
      const formattedDate = formatDate(date); // Formatea la fecha según sea necesario
      //const fechaElegida = { fecha: formattedDate };
      const formattedDate2 = date.toLocaleDateString();

      // Establece la fecha seleccionada en el contexto
      setFechaSeleccionada(formattedDate2);

      router.push(`/reservar-fecha?fecha=${formattedDate}`);


    }



  }

  const highlightedDates = birthdays.map(birthday => {
    const birthdayDate = new Date(birthday.fecha);
    return {
      month: birthdayDate.getMonth() + 1,
      day: birthdayDate.getDate(),
    };
  });

  const tileClassName = ({ date }: { date: Date }) => {
    const formattedDate = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate(),
    };
    const isBirthday = highlightedDates.some(({ month, day }) => month === formattedDate.month && day === formattedDate.day);

    const isTurno = turnos.some(turno => {
      const turnoDate = new Date(turno.fecha);
      const turnoDateString = `${turnoDate.getUTCFullYear()}-${turnoDate.getUTCMonth() + 1}-${turnoDate.getUTCDate()}`;
      return turnoDateString === `${formattedDate.year}-${formattedDate.month}-${formattedDate.day}`;
    });

    const isAgendaPendiente = agendasPendientes.some(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === `${formattedDate.year}-${formattedDate.month}-${formattedDate.day}`;
    });

    const isAgendaConfirmada = agendasConfirmadas.some(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === `${formattedDate.year}-${formattedDate.month}-${formattedDate.day}`;
    });


    if (isBirthday) {
      return 'highlighted-birthday';
    } else if (isTurno) {
      return 'highlighted-turno';
    } else if (isAgendaPendiente) {
      return 'highlighted-agenda-pendiente';
    } else if (isAgendaConfirmada) {
      return 'highlighted-agenda-confirmada';
    }
    return null;
  };

  const tileContent = ({ date, view }: { date: Date; view: string }) => {
    if (view === 'month') {
      const formattedDate = {
        month: date.getUTCMonth() + 1,
        day: date.getUTCDate(),
      };
      if (highlightedDates.some(({ month, day }) => month === formattedDate.month && day === formattedDate.day)) {
        return '🎉';
      }
    }
    return null;
  };
  //const sectionRef = useScrollAnimation();
  const { sectionRef, leftSectionRef, rightSectionRef } = useCombinedScrollAnimations();

  // Obtener la fecha de hoy
  const today = new Date();
  // Configurar minDate para que sea igual a la fecha de hoy
  const minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());

  return (

    <div ref={sectionRef} className="calendarContainerFloat scroll-animation mt-20 ">
      <div className="titleFloat">
      <h1 className="text-3xl sm:text-6xl font-bold mb-14 md:col-span-2 text-center">Agenda de Reservas</h1>
     
      </div>
      <div className="contentFloat ml-16">
        <div ref={leftSectionRef} className="cardFloat scroll-animation-left">
          <div className="floatingActionButton">
            <div className="actionItem">
              <FaCheckCircle color="red" size={40} />
              <span>Confirmado</span>
            </div>
            <div className="actionItem">
              <FaCircle color="orange" size={40} />
              <span>Cumpleaños</span>
            </div>
            <div className="actionItem">
              <FaCircle color="green" size={40} />
              <span>Pendiente</span>
            </div>
            <div className="actionItem">
              <FaClock color="purple" size={40} />
              <span>Turno</span>
            </div>
          </div>
        </div>
        <div ref={rightSectionRef} className="calendarFloat scroll-animation-right">

          <Calendar
            value={date}
            onClickDay={handleClickDay}
            minDetail="month"
            maxDetail="month"
            tileClassName={tileClassName}
            tileContent={tileContent}
            minDate={minDate} // Establecer la fecha mínima como la fecha de hoy
          />
        </div>
      </div>
    </div>

  );
}
export default AgendaReservas;