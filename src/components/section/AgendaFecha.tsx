
import React, { useState, useEffect, useContext } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { fetchBirthdaysDisponible } from '@/services/birthdayService';
import { fetchTurnos } from '@/services/turnoService';
import { fetchAgendasPendientes, fetchAgendasConfirmadas } from '@/services/agendaService';
import 'react-toastify/dist/ReactToastify.css';
import { fetchUsers } from '@/services/usuarioService';
import { fetchGroups } from '@/services/groupService';
import Agenda from '@/models/agendaModel';
import Birthday from '@/models/birthdayModel';
import Turno from '@/models/turnoModel';
import User from '@/models/usuarioModel';
import Group from '@/models/groupModel';
import useCombinedScrollAnimations from '@/hooks/useCombinedScrollAnimation';
import { useRouter } from 'next/navigation';
import { formatDate } from '@/utils/dateUtils';
import { useFechaContext } from '@/hooks/useFechaContext';

interface AgendaFechaProps {
  setFechaElegida: (fecha: string) => void; // Define el tipo de la prop setFechaElegida
}

export default function AgendaFecha({ setFechaElegida }: AgendaFechaProps) {
  const [agendasPendientes, setAgendasPendientes] = useState<Agenda[]>([]);
  const [agendasConfirmadas, setAgendasConfirmadas] = useState<Agenda[]>([]);
  const [date, setDate] = useState<Date | null>(new Date());

  const [birthdays, setBirthdays] = useState<Birthday[]>([]);
  const [turnos, setTurnos] = useState<Turno[]>([]);
  const [users, setUsers] = useState<User[]>([]);
  const [groups, setGroups] = useState<Group[]>([]);
  const router = useRouter();


  const { setFechaSeleccionada } = useFechaContext();
  const [selectedDate, setSelectedDate] = useState<Date | null>(null)
  const [show, setShow] = useState(false);
  
  useEffect(() => {
    const fetchData = async () => {
      const birthdaysData = await fetchBirthdaysDisponible();
      setBirthdays(birthdaysData);
    };
    fetchData();
  }, []);

  useEffect(() => {
    const fetchTurnosData = async () => {
      try {
        const turnosData = await fetchTurnos();
        setTurnos(turnosData);
      } catch (error) {
        console.error('Error fetching turnos:', error);
      }
    };
    fetchTurnosData();
  }, []);

  useEffect(() => {
    const fetchUsersData = async () => {
      try {
        const usersData = await fetchUsers();
        setUsers(usersData);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };
    fetchUsersData();
  }, []);

  useEffect(() => {
    const fetchGroupsData = async () => {
      try {
        const groupsData = await fetchGroups();
        setGroups(groupsData);
      } catch (error) {
        console.error('Error fetching groups:', error);
      }
    };
    fetchGroupsData();
  }, []);

  useEffect(() => {
    const fetchAgendas = async () => {
      try {
        const pendientes = await fetchAgendasPendientes();
        const confirmadas = await fetchAgendasConfirmadas();
        setAgendasPendientes(pendientes);
        setAgendasConfirmadas(confirmadas);
      } catch (error) {
        console.error('Error fetching agendas:', error);
      }
    };
    fetchAgendas();
  }, []);

  const handleClickDay = (date: Date) => {
  
    const selectedDate = new Date(date);

    // Comparando con turnos
    const selectedDateString = `${selectedDate.getUTCFullYear()}-${selectedDate.getUTCMonth() + 1}-${selectedDate.getUTCDate()}`;

    const turno = turnos.find(turno => {
      const turnoDate = new Date(turno.fecha);
      const turnoDateString = `${turnoDate.getUTCFullYear()}-${turnoDate.getUTCMonth() + 1}-${turnoDate.getUTCDate()}`;
      return turnoDateString === selectedDateString;
    });




    // Comparando con cumpleaños
    const birthday = birthdays.find(birthday => {
      const birthdayDate = new Date(birthday.fecha);
      return (
        birthdayDate.getDate() === selectedDate.getDate() &&
        birthdayDate.getMonth() === selectedDate.getMonth()
      );
    });



    const agendaPendiente = agendasPendientes.find(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === selectedDateString;
    });

 
 

    const agendaConfirmada = agendasConfirmadas.find(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === selectedDateString;
    });

  

    if (!turno && !birthday && !agendaPendiente && !agendaConfirmada) {

     // const formattedDate = formatDate(date); // Formatea la fecha según sea necesario
      //const fechaElegida = { fecha: formattedDate };
      const formattedDate2 = date.toLocaleDateString();
      const formattedDate = selectedDate.toISOString().split('T')[0]; // Formatea la fecha en YYYY-MM-DD
      setSelectedDate(selectedDate);
      setFechaElegida(formattedDate); // Guarda la fecha formateada
      console.log(formattedDate);
    }

  }

  const highlightedDates = birthdays.map(birthday => {
    const birthdayDate = new Date(birthday.fecha);
    return {
      month: birthdayDate.getMonth() + 1,
      day: birthdayDate.getDate(),
    };
  });

  const tileClassName = ({ date }: { date: Date }) => {
    const formattedDate = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate(),
    };
    const isBirthday = highlightedDates.some(({ month, day }) => month === formattedDate.month && day === formattedDate.day);

    const isTurno = turnos.some(turno => {
      const turnoDate = new Date(turno.fecha);
      const turnoDateString = `${turnoDate.getUTCFullYear()}-${turnoDate.getUTCMonth() + 1}-${turnoDate.getUTCDate()}`;
      return turnoDateString === `${formattedDate.year}-${formattedDate.month}-${formattedDate.day}`;
    });

    const isAgendaPendiente = agendasPendientes.some(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === `${formattedDate.year}-${formattedDate.month}-${formattedDate.day}`;
    });

    const isAgendaConfirmada = agendasConfirmadas.some(agenda => {
      const agendaDate = new Date(agenda.fecha);
      const agendaDateString = `${agendaDate.getUTCFullYear()}-${agendaDate.getUTCMonth() + 1}-${agendaDate.getUTCDate()}`;
      return agendaDateString === `${formattedDate.year}-${formattedDate.month}-${formattedDate.day}`;
    });


    if (isBirthday) {
      return 'highlighted-birthday';
    } else if (isTurno) {
      return 'highlighted-turno';
    } else if (isAgendaPendiente) {
      return 'highlighted-agenda-pendiente';
    } else if (isAgendaConfirmada) {
      return 'highlighted-agenda-confirmada';
    }
    return null;
  };

  const tileContent = ({ date, view }: { date: Date; view: string }) => {
    if (view === 'month') {
      const formattedDate = {
        month: date.getUTCMonth() + 1,
        day: date.getUTCDate(),
      };
      if (highlightedDates.some(({ month, day }) => month === formattedDate.month && day === formattedDate.day)) {
        return '🎉';
      }
    }
    return null;
  };
  //const sectionRef = useScrollAnimation();
  const { sectionRef, leftSectionRef, rightSectionRef } = useCombinedScrollAnimations();

  // Obtener la fecha de hoy
  const today = new Date();
  // Configurar minDate para que sea igual a la fecha de hoy
  const minDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());

  return (
    <div className="flex flex-col md:flex-row items-start md:items-center space-y-4 md:space-y-0 md:space-x-4">
    <div className="md:w-1/2">
      <label htmlFor="descripcion" className="text-xs required">
        Fecha:
      </label>
      <input 
        type="text"
        className="w-full p-2 border bg-[#3bb24d] text-white rounded-lg"
        placeholder=" "
        value={selectedDate?.toLocaleDateString() || ''}
        onFocus={() => setShow(true)}
        readOnly
      />
    </div>
    <div className="md:w-1/2 md:flex justify-end">
    <Calendar 
            value={date}
            onClickDay={handleClickDay}
            minDetail="month"
            maxDetail="month"
            tileClassName={tileClassName}
            tileContent={tileContent}
            minDate={minDate} 
          />
    </div>
  </div>
    
 
         
  );
}
