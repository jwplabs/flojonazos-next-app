import React from 'react';
import { FaFacebook, FaInstagram, FaTiktok, FaYoutube } from 'react-icons/fa'; // Importa los iconos de redes sociales
import { MdEmail } from 'react-icons/md';
import { IoLocation } from 'react-icons/io5';

export default function Footer() {
  return (
    <div className="font-puls-jakarta-sans xl:m-16 z-8 ">
      <div className="sm:flex sm:justify-between items-baseline sm:p-14 ">
        <div className="p-5">
          <p className='font-black sm:text-base xl:text-lg'>SOBRE NOSOTROS</p>
          <p className="xl:text-lg mt-3">
            Comparsa Flojonazos fundada en 1994 e coronadores del carnaval del 2019.
          </p>
        </div>
        <div className="p-5">
          <p className='font-black sm:text-base xl:text-lg'>CONTACTOS</p>
          <p className="xl:text-lg mt-3">
            <IoLocation className="inline-block w-6 h-6 text-metal" /> {/* Icono de ubicación */}
            Villa Fraterna II (Av. Piraí 6to Anillo), Santa Cruz de la Sierra, Bolivia
          </p>
          <p className="xl:text-lg mt-3">
            <MdEmail className="inline-block w-6 h-6 text-metal" /> {/* Icono de correo */}
            comparsaflojonazos@gmail.com
          </p>
        </div>
        {/* Redes sociales y correo */}
        <div className="p-5">
          <p className="text-base font-black xl:text-lg">¡Síguenos en nuestras redes sociales Flojonazos!</p>
          <div className="flex items-center mt-3">
            <a href="https://www.facebook.com/Flojonazos/" target="_blank" className="text-metal mr-3 icon">
              <FaFacebook className="w-6 h-6" /> {/* Icono de Facebook */}
            </a>
            <a href="https://www.instagram.com/explore/locations/1758238784470027/fraternidad-flojonazos/?next=%2Ftonelnes%2F&hl=es" target="_blank" className="text-metal mr-3 icon">
              <FaInstagram className="w-6 h-6" /> {/* Icono de Instagram */}
            </a>
            <a href="https://www.tiktok.com/" target="_blank" className="text-metal mr-3 icon">
              <FaTiktok className="w-6 h-6" /> {/* Icono de TikTok */}
            </a>
            <a href="https://www.youtube.com/results?search_query=flojonazos" target="_blank" className="text-metal mr-3 icon">
              <FaYoutube className="w-6 h-6" /> {/* Icono de YouTube */}
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}
