import React from 'react';

interface CustomInputProps {
  value: string;
  placeholder: string;
  onFocus: () => void;
  readOnly: boolean;
}

const CustomInput: React.FC<CustomInputProps> = ({ value, placeholder, onFocus, readOnly }) => {
  return (
    <input
      type="text"
      className="w-full p-2 flex-grow border bg-[#3bb24d] text-white rounded-lg"
      placeholder={placeholder}
      value={value}
      onFocus={onFocus}
      readOnly={readOnly}
    />
  );
};

export default CustomInput;
