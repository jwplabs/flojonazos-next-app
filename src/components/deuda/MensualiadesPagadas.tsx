import React, { useEffect, useState } from "react";
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell } from "@nextui-org/react";
import { fetchPagosByMonth } from "@/services/mensualidadPagadasService";
import PagoModel from "@/models/mensualidadPagadasModel";
import { FaCheck } from "react-icons/fa";
export default function MensualidadesPagadas() {
    const [pagos, setPagos] = useState<PagoModel[]>([]);

    useEffect(() => {
        async function fetchPagos() {
            try {
                const ci = localStorage.getItem('username') || '';
                const pagosData = await fetchPagosByMonth(ci);
                setPagos(pagosData);
            } catch (error) {
                console.error('Error fetching pagos:', error);
            }
        }

        fetchPagos();
    }, []);

    return (
        <Table aria-label="Lista de Mensualidades Pagadas">
            <TableHeader columns={[{ key: "fecha", label: "FECHA" }, { key: "costo", label: "COSTO" }, { key: "estado", label: "ESTADO" }]}>
                {(column) => (
                    <TableColumn key={column.key} className="text-left">{column.label}</TableColumn>
                )}
            </TableHeader>
            <TableBody items={pagos}>
                {(item) => (
                    <TableRow key={item.fecha}>
                        <TableCell className="text-left" style={{ backgroundColor: 'green', color: 'white' }}>{item.fecha}</TableCell>
                        <TableCell className="text-left" style={{ backgroundColor: 'green', color: 'white' }}>{item.monto}</TableCell>
                        <TableCell className="text-left" style={{ backgroundColor: 'green', color: 'white' }}> <FaCheck /> </TableCell>
                    </TableRow>
                )}
            </TableBody>

        </Table>
    );
}
