import React from "react";
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell, getKeyValue } from "@nextui-org/react";

const rows = [
  {
    key: "1",
    fecha: "Tony Reichert",
    monto: "CEO",
    estado: "Active",
  },
  {
    key: "2",
    fecha: "Zoey Lang",
    monto: "Technical Lead",
    estado: "Paused",
  },
  {
    key: "3",
    fecha: "Jane Fisher",
    monto: "Senior Developer",
    estado: "Active",
  },
  {
    key: "4",
    fecha: "William Howard",
    monto: "Community Manager",
    estado: "Vacation",
  },
];

const columns = [
  {
    key: "fecha",
    label: "FECHA",
  },
  {
    key: "monto",
    label: "MONTO",
  },
  {
    key: "estado",
    label: "ESTADO",
  },
];

export default function MensualidadesTable() {
  return (
    <Table aria-label="Example table with dynamic content">
      <TableHeader columns={columns}>
        {(column) => (
          <TableColumn key={column.key} className="text-left">{column.label}</TableColumn>
        )}
      </TableHeader>
      <TableBody items={rows}>
        {(item) => (
          <TableRow key={item.key}>
            {(columnKey) => (
              <TableCell className="text-left">{getKeyValue(item, columnKey)}</TableCell>
            )}
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
}
