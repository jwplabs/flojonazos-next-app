import React from 'react';
import { ButtonAceptar } from '../ButtonAceptar';
import Image from 'next/image';
import ButtonDownload from '../ButtonDownload';
import { createAgenda } from '@/services/agendaService';
import { toast } from 'sonner';
import { useRouter } from 'next/navigation';
import { sendEmail } from '@/utils/sendEmailUtils';



const QrMensualidad: React.FC<{ }> = ({ }) => {
  const router = useRouter();

  const handleClickAceptar = async () => {
    router.push(`/`);
  };

  // Renderiza el botón solo si todos los campos requeridos de datosReserva tienen valores válidos
  const renderizarBotonAceptar = () => {
    
   
      return (
        <ButtonAceptar buttonText="Aceptar" onClick={handleClickAceptar} />
      );
    
   
  };

  return (
    <div className="flex">
      <div className="flex-grow bg-gray-100">
        <div className="container mx-auto p-4 lg:mt-0 lg:pl-76">
          <div className="bg-white rounded shadow p-4">
            <h1 className="text-2xl font-semibold mb-4">QR Mensualidad</h1>
 
            <img className="card__img" src="https://res.cloudinary.com/dci37dfd7/image/upload/v1713135429/frater/qr_frater/2bdc0588_xkusyb.jpg" alt="Card Image" width={350} height={350} />
            <ButtonDownload url="https://res.cloudinary.com/dci37dfd7/image/upload/v1713135429/frater/qr_frater/2bdc0588_xkusyb.jpg" />
            <div className="footer flex justify-end">
              {/* Renderiza el botón de aceptar condicionalmente */}
              {renderizarBotonAceptar()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default QrMensualidad;
