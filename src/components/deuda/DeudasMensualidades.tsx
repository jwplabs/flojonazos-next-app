import React from 'react'
import MensualidadesTable from './MensualidadesTable'
import MensualidadesPagadas from './MensualiadesPagadas';
import MensualidadesNoPagadas from './MensualidadesNoPagadas';

export default function Deudas() {
  return (
    <div className="flex">
      <div className="flex-grow bg-gray-100">
        <div className="container mx-auto p-4 lg:mt-0 lg:pl-76">
          <div className="bg-white rounded shadow p-4">
            <h1 className="text-2xl font-semibold mb-4">Mensualidades</h1>
 
           
            <MensualidadesPagadas/>
            <MensualidadesNoPagadas/>
         
          </div>
        </div>
      </div>
    </div>
  );
};
