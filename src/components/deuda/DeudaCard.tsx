
import React, { useEffect, useState } from "react";
import { Card, CardHeader, CardFooter, Image, Button } from "@nextui-org/react";
import useCombinedScrollAnimations from "@/hooks/useCombinedScrollAnimation";
import HoverButton from "../HoverButton";
import { useRouter } from "next/navigation";
import { fetchDeudasByMonth } from "@/services/deudaService";
import DeudaExtraordinariaModel, { Extraordinaria } from '@/models/deudaExModel';
import { fetchCurrentDeudaExtraordinaria } from "@/services/deudaExService";


export default function App() {
  //const sectionRef = useScrollAnimation();

  const [currentDeuda, setCurrentDeuda] = useState(new DeudaExtraordinariaModel(new Extraordinaria(0, 0, '', ''), 0, 0));

  const [totalDeuda, setTotalDeuda] = useState(0);
  const { sectionRef, leftSectionRef, rightSectionRef } = useCombinedScrollAnimations();
  const router = useRouter();

  useEffect(() => {
    async function fetchData() {
      try {
        const { total } = await fetchDeudasByMonth();
        setTotalDeuda(total);
      } catch (error) {
        console.error('Error fetching deudas:', error);
      }
    }
    fetchData();
  }, []);


  useEffect(() => {
    async function fetchData() {
      try {
        const deudaData = await fetchCurrentDeudaExtraordinaria();
        setCurrentDeuda(deudaData);
      } catch (error) {
        console.error('Error fetching current deuda extraordinaria:', error);
      }
    }
    fetchData();
  }, []);

  const handleClick = () => {
    // Al hacer clic en el botón, guardamos el id del evento en el estado local

    //router.push('/reserva');
    router.push(`/detalle-extra`);
  };
  const handleClickDetalles = () => {
    // Al hacer clic en el botón, guardamos el id del evento en el estado local

    //router.push('/reserva');
    router.push(`/deuda-mensualidad`);
  };
  const handleClickQrMensualidades = () => {
    // Al hacer clic en el botón, guardamos el id del evento en el estado local

    //router.push('/reserva');
    router.push(`/qr-mensualidad`);
  };
  const handleClickQrExtraordinaria = () => {
    // Al hacer clic en el botón, guardamos el id del evento en el estado local

    //router.push('/reserva');
    router.push(`/qr-extraordinaria`);
  };
  return (
    <div ref={sectionRef} className="calendarContainerFloat scroll-animation flex justify-center items-center min-h-screen mt-6">
      <div className="max-w-[1200px] gap-16 grid grid-cols-1 md:grid-cols-2 px-8">
        <h1 className="text-3xl sm:text-6xl font-bold mb-14 md:col-span-2 text-center">Deudas</h1>

        <div ref={leftSectionRef} className="scroll-animation-left">
          <div className="p-4 md:p-8 flex justify-center items-center">
            <Card className="card-responsive rounded-lg shadow-lg w-full sm:w-[400px] md:w-[450px] h-[350px]">
              <CardHeader className="absolute z-10 top-1 flex-col items-start">
                <p className="text-tiny text-white/60 uppercase font-bold">DEUDA MENSUALIDAD</p>
                <h4 className="text-black font-medium text-2xl">{`Total: ${totalDeuda !== null ? totalDeuda : 'Cargando...'}`} bs.</h4>
              </CardHeader>
              {/* <img
              alt="Card example background"
              className="z-0 w-full h-full scale-125 -translate-y-6 object-cover rounded-lg"
              src="https://i.postimg.cc/qqjPVgzL/20240530-193822.png"
            />*/}
              <div className="background-gradient z-0 w-full h-full scale-125 -translate-y-6 object-cover rounded-lg"></div>
              <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between rounded-b-lg">
                <div>
                  <HoverButton label="Detalles" onClick={handleClickDetalles} />
                </div>
                <HoverButton label="Qr pagar" onClick={handleClickQrMensualidades} />
              </CardFooter>
            </Card>
          </div>
        </div>

        <div ref={rightSectionRef} className="scroll-animation-right">
          <div className="p-4 md:p-8 flex justify-center items-center">
            <Card className="card-responsive rounded-lg shadow-lg w-full sm:w-[400px] md:w-[450px] h-[350px]">
              <CardHeader className="absolute z-10 top-1 flex-col items-start">
                <p className="text-tiny text-white/60 uppercase font-bold">DEUDA EXTRAORDINARIA</p>
                <h4 className="text-black font-medium text-2xl">{`Saldo: ${currentDeuda.saldo}`} bs.</h4>
                <h4 className="text-black font-medium text-2xl">{`Deuda: ${currentDeuda.deuda}`} bs.</h4>
              </CardHeader>
              {/* <img
              alt="Card example background"
              className="z-0 w-full h-full scale-125 -translate-y-6 object-cover rounded-lg"
              src="https://i.postimg.cc/qqjPVgzL/20240530-193822.png"
            />*/}
              <div className="background-gradient z-0 w-full h-full scale-125 -translate-y-6 object-cover rounded-lg"></div>
              <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between rounded-b-lg">
                <div>
                  <HoverButton label="Detalles" onClick={handleClick} />
                </div>
                <HoverButton label="Qr pagar" onClick={handleClickQrExtraordinaria} />
              </CardFooter>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );

}
