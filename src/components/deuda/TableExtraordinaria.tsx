import React, { useEffect, useState } from 'react';
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell } from '@nextui-org/react';
import { getUserPayments } from '@/services/extraordinariaDetalleService'; // Importa la función para obtener los pagos extraordinarios
import PayModel from '@/models/payModel';

const TableExtraordinaria: React.FC = () => {
  const [payments, setPayments] = useState<PayModel[]>([]);

  useEffect(() => {
    async function fetchPayments() {
      try {
        const paymentsData = await getUserPayments();
        setPayments(paymentsData);
      } catch (error) {
        console.error('Error fetching payments:', error);
      }
    }

    fetchPayments();
  }, []);

  return (
    <Table aria-label="Detalle de Pagos Extraordinarios">
      <TableHeader columns={[{ key: "fecha_pago", label: "Fecha de Pago" }, { key: "monto_pagado", label: "Monto Pagado" }, { key: "deuda_extraordinaria", label: "Deuda Extraordinaria" }]}>
        {(column) => (
          <TableColumn key={column.key} className="text-left">{column.label}</TableColumn>
        )}
      </TableHeader>
      <TableBody items={payments}>
        {(item) => (
          <TableRow key={item.id}>
            <TableCell className="text-left" style={{ backgroundColor: 'red', color: 'white' }}>{item.fecha_pago}</TableCell>
            <TableCell className="text-left" style={{ backgroundColor: 'red', color: 'white' }}>{item.monto_pagado}</TableCell>
            <TableCell className="text-left" style={{ backgroundColor: 'red', color: 'white' }}>{item.deuda_extraordinaria}</TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
}

export default TableExtraordinaria;
