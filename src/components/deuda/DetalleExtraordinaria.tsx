import React, { useEffect, useState } from 'react';
import { getUserPayments } from '@/services/extraordinariaDetalleService'; // Importa la función para obtener los pagos extraordinarios
import PayModel from '@/models/payModel';
import TableExtraordinaria from './TableExtraordinaria';



export default function DetalleExtraordinaria() {
  const [payments, setPayments] =useState<PayModel[]>([]);

  useEffect(() => {
    async function fetchPayments() {
      try {
        const paymentsData = await getUserPayments();
        console.log(paymentsData);
        console.log('este es');
        setPayments(paymentsData);
      } catch (error) {
        console.error('Error fetching payments:', error);
      }
    }

    fetchPayments();
  }, []);
  return (
    <div className="flex">
      <div className="flex-grow bg-gray-100">
        <div className="container mx-auto p-4 lg:mt-0 lg:pl-76">
          <div className="bg-white rounded shadow p-4">
            <h1 className="text-2xl font-semibold mb-4">Detalle Extraordinaria</h1>
 
            <TableExtraordinaria/>
          
         
          </div>
        </div>
      </div>
    </div>
  );
};

