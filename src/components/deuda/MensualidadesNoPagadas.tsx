import React, { useEffect, useState } from "react";
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell } from "@nextui-org/react";
import { fetchDeudasByMonth2 } from "@/services/deudaService"; // Importa tu función de servicio para obtener las deudas
import DeudaModel from "@/models/deudaModel";
import { FaRegTimesCircle } from "react-icons/fa";

export default function MensualidadesNoPagadas() {
    const [deudas, setDeudas] = useState<DeudaModel[]>([]); // Estado para almacenar las deudas

    useEffect(() => {
        // Llama a la función de servicio para obtener las deudas
        async function fetchDeudas() {
            try {
                const deudasData = await fetchDeudasByMonth2();
                setDeudas(deudasData);
            } catch (error) {
                console.error('Error fetching deudas:', error);
            }
        }

        fetchDeudas(); // Invoca la función de obtención de deudas al cargar el componente
    }, []);

    return (
        <Table aria-label="Lista de Deudas No Pagadas">
            <TableHeader columns={[{ key: "fecha", label: "FECHA" }, { key: "costo", label: "COSTO" }, { key: "estado", label: "ESTADO" }]}>
                {(column) => (
                    <TableColumn key={column.key} className="text-left">{column.label}</TableColumn>
                )}
            </TableHeader>
            <TableBody items={deudas}>
                {(item) => (
                    <TableRow key={item.fecha}>
                        <TableCell className="text-left" style={{ backgroundColor: 'red', color: 'white' }}>{item.fecha}</TableCell>
                        <TableCell className="text-left" style={{ backgroundColor: 'red', color: 'white' }}>{item.costo}</TableCell>
                        <TableCell className="text-left" style={{ backgroundColor: 'red', color: 'white' }}><FaRegTimesCircle /></TableCell>
                    </TableRow>
                )}
            </TableBody>

        </Table>
    );
}
