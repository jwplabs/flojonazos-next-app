import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import { fetchEventTypes } from '@/services/eventTypeService';
import EventType from '@/models/eventTypeModel';

interface SelectEventTypeProps {
  onChange: (selectedOptionId: number) => void; // Función de devolución de llamada para manejar la selección de eventos
}

const SelectEventType: React.FC<SelectEventTypeProps> = ({ onChange }) => {
    const [eventos, setEventos] = useState<EventType[]>([]);
  
    useEffect(() => {
      const loadEventos = async () => {
        try {
          const eventTypes = await fetchEventTypes();
          setEventos(eventTypes);
        } catch (error) {
          console.error('Error fetching event types:', error);
        }
      };
  
      loadEventos();
    }, []);
  
    return (
      <Select
        options={eventos.map((evento: EventType) => ({
          value: evento.id,
          label: evento.nombre+' Costo: entre semana '+evento.costo_entresemana+', fin de semana '+evento.costo_finsemana
         
        }))}
        onChange={(selectedOption) => {
          if (selectedOption) {
            onChange(selectedOption.value); // Devuelve solo el valor (ID) del evento seleccionado
          } else {
            // Aquí manejar el caso en el que no se selecciono ninguna opcion
          }
        }}
        styles={{
          control: provided => ({
            ...provided,
            backgroundColor: '#3bb24d', // Color de fondo verde
            color: 'white', // Color de texto blanco
            border: '1px solid white', // Borde blanco de 1px
            borderRadius: '5px' // Bordes redondeados
          }),
          option: provided => ({
            ...provided,
            backgroundColor: '#3bb24d',
            color: 'white' // Color de texto blanco para las opciones
          }),
          singleValue: provided => ({
            ...provided,
            color: 'white' // Color de texto blanco para el valor seleccionado
          })
        }}
      />
    );
  };
export default SelectEventType;
