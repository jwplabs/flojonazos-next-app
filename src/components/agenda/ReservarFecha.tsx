import React, { useEffect, useState } from 'react';
import { ButtonAceptar } from '../ButtonAceptar';
import MyEventos from './SelectEventType';
import { useQrContext } from '@/hooks/useQrContext';
import { useRouter } from 'next/navigation';
import { fetchEventTypePriceById } from '@/services/eventTypeService';
import { fetchQRData, findQRByValue } from '@/services/qrService';
import Spinner from '../Spinner';

interface FechaElegida {
  fecha: string;
}

const ReservarFecha: React.FC<{ fechaElegida: FechaElegida }> = ({ fechaElegida }) => {
  const [valorCampoDescripcion, setValorCampoDescripcion] = useState('');
  const { costo,setFecha, setDescripcion, setEsEntresemana, setTipoEvento, setCosto, setUrlQr } = useQrContext();
  const [EventId, setSelectedEventId] = useState(0);
  const router = useRouter();
  
  const [qrData, setQRData] = useState([]);
  const [searchedValue, setSearchedValue] = useState('');
  const [foundURL, setFoundURL] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Cargar los datos de los códigos QR al montar el componente
    async function fetchData() {
      try {
        const data = await fetchQRData();
        setQRData(data);
        setLoading(false); // Una vez cargados los datos, cambia el estado de carga a falso
      } catch (error) {
        console.error('Error fetching QR data:', error);
        setLoading(false); // En caso de error, también cambia el estado de carga a falso
      }
    }
    fetchData();
  }, []);

  // Función para verificar si una fecha es entre semana o no
  const esEntreSemana = (fecha: string): boolean => {
    const date = new Date(fecha);
    const day = date.getDay(); // 0 para domingo, 1 para lunes, ..., 6 para sábado
    return day >= 0 && day <= 4; // Devuelve true si es entre semana (de lunes a viernes), false en caso contrario
  }; 

  const handleEventTypeSelection = async (selectedEventId: number) => {
    // Aquí puedes manejar la lógica relacionada con la selección de eventos
    console.log('Evento seleccionado:', selectedEventId);
    setTipoEvento(selectedEventId); // Establece el ID del evento seleccionado
   // toast(selectedEventId);
    setSelectedEventId(selectedEventId);
    const entreSemana = esEntreSemana(fechaElegida.fecha);
    setEsEntresemana(entreSemana);
    try {
      // Llama a fetchEventTypePriceById para obtener el precio
      const precio = await fetchEventTypePriceById(selectedEventId, entreSemana);
  
      // Aquí puedes manejar el precio obtenido como un string
      const precioString = precio.toString();
      setCosto(precioString);
      console.log('Precio:', precioString);
         // Guarda el precio en el estado o realiza cualquier otra acción necesaria
    setSearchedValue(costo);
      
    const url = findQRByValue(precioString, qrData);
      
    console.log('Url:', url);
    if (url) {
      setFoundURL(url);
      setUrlQr(url);
    } else {
      setFoundURL('No se encontró ningún código QR con ese valor.');
    }
    
    } catch (error) {
      console.error('Error al obtener el precio:', error);
      // Aquí puedes manejar el error
    }

 
  };

  const handleClick = () => {
    setDescripcion(valorCampoDescripcion);
    setFecha(fechaElegida.fecha);
    //const entreSemana = esEntreSemana(fechaElegida.fecha);
    //setEsEntresemana(entreSemana);
      router.push(`/qr`);
  };

   // Muestra el spinner si los datos aún se están cargando
   if (loading) {
    return <Spinner />;
  }
  return (
    <div className="flex">
      <div className="flex-grow bg-gray-100">
        <div className="container mx-auto p-4 lg:mt-0 lg:pl-76">
          <div className="bg-white rounded shadow p-4">
            <h1 className="text-2xl font-semibold mb-4">Reservar</h1>
           
              <div className="mb-4">
                <label htmlFor="fecha" className="text-xs required">
                  Fecha Elegida: {fechaElegida.fecha}
                </label>
              </div>
              <div className="mb-4">
                <label htmlFor="descripcion" className="text-xs required">
                  Observación (Nombre del pagante):
                </label>
                <input
                  type="text"
                  id="descripcion"
                  name="descripcion"
                  className="w-full p-2 flex-grow border rounded-lg bg-[#3bb24d] text-white"
                  value={valorCampoDescripcion} // Usa el valor del estado local para el campo de entrada
                  onChange={(e) => setValorCampoDescripcion(e.target.value)} // Maneja el cambio en el campo de entrada
                  required
                />
              </div>
              <div className="mb-4">
                <label htmlFor="evento" className="text-xs required">
                  Tipo de Evento:
                </label>
                <MyEventos onChange={handleEventTypeSelection}/>
              </div>
              <div className="footer flex justify-end">
                <ButtonAceptar buttonText="Aceptar" onClick={handleClick} />
                
              </div>
          
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReservarFecha;
