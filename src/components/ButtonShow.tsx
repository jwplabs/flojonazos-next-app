import React, { FC, useState } from 'react';

interface ButtonShowProps {
  buttonText?: string;
  onClick?: () => void;
  href?: string; // Nueva propiedad para la URL
}

export const ButtonShow: FC<ButtonShowProps> = ({ buttonText = 'Mostrar', onClick, href }) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleClick = () => {
    if (href) {
      window.location.href = href; // Redirige a la URL si está definida
    } else if (onClick) {
      onClick(); // Ejecuta la función onClick si está definida
    }
  };

  return (
    <button
      className={`bg-midgreen hover:bg-indigo-600 text-white font-semibold px-2 py-1 rounded responsive-button lg:inline-block lg:mr-2 mb-2 hover-show ${isHovered ? 'button' : ''}`}
      onClick={handleClick} // Usamos handleClick en lugar de onClick directamente
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {buttonText}
    </button>
  );
};
