import { useEffect, useRef } from 'react';

const useScrollAnimationRight = () => {
  const sectionRef = useRef<HTMLDivElement>(null);

  const handleIntersection = (entries: IntersectionObserverEntry[]) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        sectionRef.current?.classList.add('visible-right');
      } else {
        sectionRef.current?.classList.remove('visible-right');
      }
    });
  };

  useEffect(() => {
    const observer = new IntersectionObserver(handleIntersection, {
      root: null,
      rootMargin: '0px',
      threshold: 0.5,
    });

    if (sectionRef.current) {
      observer.observe(sectionRef.current);
    }

    return () => {
      if (sectionRef.current) {
        observer.unobserve(sectionRef.current);
      }
    };
  }, []);

  return sectionRef;
};

export default useScrollAnimationRight;
