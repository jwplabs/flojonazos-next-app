import { useContext } from 'react';
import FechaContext, { FechaContextType } from '@/providers/FechaContextProvider';

export const useFechaContext = (): FechaContextType => {
  const context = useContext(FechaContext);
  if (!context) {
    throw new Error('useFechaContext debe ser usado dentro de un proveedor FechaProvider');
  }
  return context;
};
