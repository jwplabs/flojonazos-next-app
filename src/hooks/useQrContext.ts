import { useContext } from 'react';
import QrContext, { QrContextType } from '@/providers/QrContextProvider';

export const useQrContext = (): QrContextType => {
  const context = useContext(QrContext);
  if (!context) {
    throw new Error('useQrContext debe ser usado dentro de un proveedor QrProvider');
  }
  return context;
};
