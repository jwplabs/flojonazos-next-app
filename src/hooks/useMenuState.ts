import { useState } from 'react';

const useMenuState = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [hoveredItem, setHoveredItem] = useState<string | null>(null);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  const handleMouseEnter = (item: string) => {
    setHoveredItem(item);
  };

  const handleMouseLeave = () => {
    setHoveredItem(null);
  };

  return {
    menuOpen,
    toggleMenu,
    hoveredItem,
    handleMouseEnter,
    handleMouseLeave,
  };
};

export default useMenuState;
