import { useEffect, useRef } from 'react';

const useCombinedScrollAnimations = () => {
  const sectionRef = useRef<HTMLDivElement>(null);
  const leftSectionRef = useRef<HTMLDivElement>(null);
  const rightSectionRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleIntersection = (entries: IntersectionObserverEntry[]) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          sectionRef.current?.classList.add('visible');
        } else {
          sectionRef.current?.classList.remove('visible');
        }
      });
    };

    const handleLeftIntersection = (entries: IntersectionObserverEntry[]) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          leftSectionRef.current?.classList.add('visible-left');
        } else {
          leftSectionRef.current?.classList.remove('visible-left');
        }
      });
    };

    const handleRightIntersection = (entries: IntersectionObserverEntry[]) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          rightSectionRef.current?.classList.add('visible-right');
        } else {
          rightSectionRef.current?.classList.remove('visible-right');
        }
      });
    };

    const observer = new IntersectionObserver(handleIntersection, {
      root: null,
      rootMargin: '0px',
      threshold: 0.5,
    });

    const leftObserver = new IntersectionObserver(handleLeftIntersection, {
      root: null,
      rootMargin: '0px',
      threshold: 0.5,
    });

    const rightObserver = new IntersectionObserver(handleRightIntersection, {
      root: null,
      rootMargin: '0px',
      threshold: 0.5,
    });
 
    if (sectionRef.current) {
      observer.observe(sectionRef.current);
    }

    if (leftSectionRef.current) {
      leftObserver.observe(leftSectionRef.current);
    }
    
    if (rightSectionRef.current) {
      rightObserver.observe(rightSectionRef.current);
    }
    return () => {
      if (sectionRef.current) {
        observer.unobserve(sectionRef.current);
      }

      if (leftSectionRef.current) {
        leftObserver.unobserve(leftSectionRef.current);
      }
      if (rightSectionRef.current) {
        rightObserver.unobserve(rightSectionRef.current);
      }
    };
  }, []);

  return { sectionRef, leftSectionRef, rightSectionRef };
};

export default useCombinedScrollAnimations;
