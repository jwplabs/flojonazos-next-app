
import { fetchBirthdaysDisponible } from '@/services/birthdayService';

export async function getBirthdays() {
  try {
    const birthdaysData = await fetchBirthdaysDisponible();
    return birthdaysData;
  } catch (error) {
    console.error('Error fetching birthdays:', error);
    return [];
  }
}
