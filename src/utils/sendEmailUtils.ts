import { fetchLastTesoreroEmail } from '@/services/usuarioService';
import { createTransport } from 'nodemailer';

export async function sendEmail(fecha:string, observacion: string) {
  // Obtener los datos del usuario y la fecha de la reserva del almacenamiento local
  const userName = localStorage.getItem('userName');
  const name = localStorage.getItem('name');
  const formattedFecha2 = fecha;
  // Obtener el correo electrónico del tesorero
  const tesoreroemail = await fetchLastTesoreroEmail();
  
  // Configurar el servidor SMTP
  const transporter = createTransport({
    service: 'gmail',
    auth: {
      user: 'jwplabs@gmail.com',
      pass: 'wuyedzrwmwofzkud'
    }
  });

  // Crear el mensaje de correo electrónico en formato HTML
  const mailOptions = {
    from: 'Flojonazos App <jwplabs@gmail.com>',
    //to: tesoreroemail,
    to: 'luhpelayo@gmail.com',
    subject: 'Fraternidad Flojonazos',
    html: `
      <h1>Verificar pago para reserva</h1>
      <p>Hola Tesorero,</p>
      <p>Por favor, verificar el pago del usuario ${userName} realizado el ${formattedFecha2} para reservar fecha. 
      A continuación, los detalles:</p>
      <table border="1" style="background-color: #b2ebf2;">
        <tr>
          <th>Usuario</th>
          <th>Fecha</th>
          <th>Observación(Nombre del pagante)</th>
        </tr>
        <tr>
          <td>${name}</td>
          <td>${formattedFecha2}</td>
          <td>${observacion}</td>
        </tr>
      </table>
      <p>Gracias,</p>
      <p>Flojonazos App</p>
    `
  };

  try {
    // Enviar el correo electrónico
    const info = await transporter.sendMail(mailOptions);
    console.log('Correo electrónico enviado:', info.messageId);
  } catch (error) {
    console.error('Error al enviar el correo electrónico:', error);
    throw error;
  }
}
