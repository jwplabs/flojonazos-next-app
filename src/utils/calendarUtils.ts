
export function findBirthdayByDate(birthdays: any, selectedDate:any) {
    const selectedDateObj = new Date(selectedDate);
    return birthdays.find((birthday: any) => {
      const birthdayDate = new Date(birthday.fecha);
      return (
        birthdayDate.getDate() === selectedDateObj.getDate() &&
        birthdayDate.getMonth() === selectedDateObj.getMonth() 
      );
    });
  }
  
  export function getHighlightedDates(birthdays: any) {
    return birthdays.map((birthday:any) => new Date(birthday.fecha));
  }
  