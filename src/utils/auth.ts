export const isAuthenticated = () => {
    const username = localStorage.getItem('username');
    const accessToken = localStorage.getItem('accessToken');
    const refreshToken = localStorage.getItem('refreshToken');
    return username && accessToken && refreshToken;
  };
  