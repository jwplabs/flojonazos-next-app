export function getCurrentUser() {
    // Obtener datos del usuario del almacenamiento local
    const username = localStorage.getItem('username');
    const email = localStorage.getItem('email');
  
    return { username, email };
  }
  