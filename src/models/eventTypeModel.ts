class EventType {
  id: number;
  nombre: string;
  descripcion: string;
  costo_entresemana: number;
  costo_finsemana: number;

  constructor(id: number, nombre: string, descripcion: string, costo_entresemana: number, costo_finsemana: number) {
      this.id = id;
      this.nombre = nombre;
      this.descripcion = descripcion;
      this.costo_entresemana = costo_entresemana;
      this.costo_finsemana = costo_finsemana;
  }

  static fromJson(eventTypeJson: {
      id: number;
      nombre: string;
      descripcion: string;
      costo_entresemana: number;
      costo_finsemana: number;
  }) {
      const { id, nombre, descripcion, costo_entresemana, costo_finsemana } = eventTypeJson;
      return new EventType(id, nombre, descripcion, costo_entresemana, costo_finsemana);
  }
}

export default EventType;
