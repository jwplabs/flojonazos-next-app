class QRModel {
    id: number;
    qr_valor: string;
    url: string;
    descripcion: string;
    tipo_evento: number;
  
    constructor(id: number, qr_valor: string, url: string, descripcion: string, tipo_evento: number) {
        this.id = id;
        this.qr_valor = qr_valor;
        this.url = url;
        this.descripcion = descripcion;
        this.tipo_evento = tipo_evento;
    }
  
    static fromJson(qrJson: any) {
        const { id, qr_valor, url, descripcion, tipo_evento } = qrJson;
        return new QRModel(id, qr_valor, url, descripcion, tipo_evento);
    }
  }
  
  export default QRModel;
  