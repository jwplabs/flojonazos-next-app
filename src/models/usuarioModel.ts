export default class User {
  id: string;
  last_login: Date | null;
  username: string;
  ci: boolean;
  full_name: string;
  email: string;
  phone: string;
  financial_condition: string;
  role: string;
  copy_ci: boolean;
  avatar: boolean;
  suspend: boolean;
  is_staff: boolean;
  is_active: boolean;
  is_admin: boolean;
  is_superuser: boolean;
  verified: boolean;
  groups: any[];
  user_permissions: any[];

  constructor({
      id,
      last_login,
      username,
      ci,
      full_name,
      email,
      phone,
      financial_condition,
      role,
      copy_ci,
      avatar,
      suspend,
      is_staff,
      is_active,
      is_admin,
      is_superuser,
      verified,
      groups,
      user_permissions,
  }: {
      id: string;
      last_login: Date | null;
      username: string;
      ci: boolean;
      full_name: string;
      email: string;
      phone: string;
      financial_condition: string;
      role: string;
      copy_ci: boolean;
      avatar: boolean;
      suspend: boolean;
      is_staff: boolean;
      is_active: boolean;
      is_admin: boolean;
      is_superuser: boolean;
      verified: boolean;
      groups: any[];
      user_permissions: any[];
  }) {
      this.id = id || '';
      this.last_login = last_login || null;
      this.username = username || '';
      this.ci = ci || false;
      this.full_name = full_name || '';
      this.email = email || '';
      this.phone = phone || '';
      this.financial_condition = financial_condition || '';
      this.role = role || '';
      this.copy_ci = copy_ci || false;
      this.avatar = avatar || false;
      this.suspend = suspend || false;
      this.is_staff = is_staff || false;
      this.is_active = is_active || false;
      this.is_admin = is_admin || false;
      this.is_superuser = is_superuser || false;
      this.verified = verified || false;
      this.groups = groups || [];
      this.user_permissions = user_permissions || [];
  }

  static fromJson(json: {
      id: string;
      last_login: Date | null;
      username: string;
      ci: boolean;
      full_name: string;
      email: string;
      phone: string;
      financial_condition: string;
      role: string;
      copy_ci: boolean;
      avatar: boolean;
      suspend: boolean;
      is_staff: boolean;
      is_active: boolean;
      is_admin: boolean;
      is_superuser: boolean;
      verified: boolean;
      groups: any[];
      user_permissions: any[];
  }) {
      return new User({
          id: json.id,
          last_login: json.last_login,
          username: json.username,
          ci: json.ci,
          full_name: json.full_name,
          email: json.email,
          phone: json.phone,
          financial_condition: json.financial_condition,
          role: json.role,
          copy_ci: json.copy_ci,
          avatar: json.avatar,
          suspend: json.suspend,
          is_staff: json.is_staff,
          is_active: json.is_active,
          is_admin: json.is_admin,
          is_superuser: json.is_superuser,
          verified: json.verified,
          groups: json.groups,
          user_permissions: json.user_permissions,
      });
  }
}
