export default class Agenda {
  id: number;
  fecha: Date;
  horaInicio: string;
  horaFin: string;
  descripcion: string;
  esEntreSemana: boolean;
  estadoReserva: string;
  createdDate: Date;
  tipoEvento: number;
  user: string;

  constructor({ id, fecha, horaInicio, horaFin, descripcion, esEntreSemana, estadoReserva, createdDate, tipoEvento, user }: {
    id: number;
    fecha: Date;
    horaInicio: string;
    horaFin: string;
    descripcion: string;
    esEntreSemana: boolean;
    estadoReserva: string;
    createdDate: Date;
    tipoEvento: number;
    user: string;
  }) {
    this.id = id || 0;
    this.fecha = fecha || new Date();
    this.horaInicio = horaInicio || '';
    this.horaFin = horaFin || '';
    this.descripcion = descripcion || '';
    this.esEntreSemana = esEntreSemana || false;
    this.estadoReserva = estadoReserva || '';
    this.createdDate = createdDate || new Date();
    this.tipoEvento = tipoEvento || 0;
    this.user = user || '';
  }

  static fromJson(json: {
    id: number;
    fecha: string | number | Date;
    hora_inicio: string;
    hora_fin: string;
    descripcion: string;
    es_entresemana: boolean;
    estado_reserva: string;
    created_date: string | number | Date;
    tipo_evento: number;
    user: string;
  }) {
    return new Agenda({
      id: json.id || 0,
      fecha: new Date(json.fecha),
      horaInicio: json.hora_inicio || '',
      horaFin: json.hora_fin || '',
      descripcion: json.descripcion || '',
      esEntreSemana: json.es_entresemana || false,
      estadoReserva: json.estado_reserva || '',
      createdDate: new Date(json.created_date),
      tipoEvento: json.tipo_evento || 0,
      user: json.user || '',
    });
  }
}
