class DebtModel {
    mes: string;
    gestion: number;
    costo: number;
    fecha: string;
    mensualidad: number;
  
    constructor(mes: string, gestion: number, costo: number, fecha: string, mensualidad: number) {
        this.mes = mes;
        this.gestion = gestion;
        this.costo = costo;
        this.fecha = fecha;
        this.mensualidad = mensualidad;
    }
  
    static fromJson(debtJson: any) {
        const { mes, gestion, costo, fecha, mensualidad } = debtJson;
        return new DebtModel(mes, gestion, costo, fecha, mensualidad);
    }
  }
  
  export default DebtModel;
  