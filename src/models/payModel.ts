// payModel.ts

class PayModel {
    id: number;
    fecha_pago: string;
    monto_pagado: number;
    deuda_extraordinaria: number;
  
    constructor(id: number, fecha_pago: string, monto_pagado: number, deuda_extraordinaria: number) {
      this.id = id;
      this.fecha_pago = fecha_pago;
      this.monto_pagado = monto_pagado;
      this.deuda_extraordinaria = deuda_extraordinaria;
    }
  
    static fromJson(paymentJson: any) {
      const { id, fecha_pago, monto_pagado, deuda_extraordinaria } = paymentJson;
      return new PayModel(id, fecha_pago, monto_pagado, deuda_extraordinaria);
    }
  }
  
  export default PayModel;
  