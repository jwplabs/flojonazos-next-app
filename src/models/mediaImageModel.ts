export default class MediaImage {
  id: number;
  url: string;
  descripcion: string;
  secuencia: number;
  mostrar: boolean;
  uploadDate: Date;
  fraternidad: number;

  constructor({ id, url, descripcion, secuencia, mostrar, upload_date, fraternidad }: { id: number; url: string; descripcion: string; secuencia: number; mostrar: boolean; upload_date: string | number | Date; fraternidad: number }) {
      this.id = id || 0;
      this.url = url || '';
      this.descripcion = descripcion || '';
      this.secuencia = secuencia || 0;
      this.mostrar = mostrar || false;
      this.uploadDate = new Date(upload_date);
      this.fraternidad = fraternidad || 0;
  }

  static fromJson(json: { id: number; url: string; descripcion: string; secuencia: number; mostrar: boolean; upload_date: string | number | Date; fraternidad: number }) {
      return new MediaImage({
          id: json.id,
          url: json.url,
          descripcion: json.descripcion,
          secuencia: json.secuencia,
          mostrar: json.mostrar,
          upload_date: json.upload_date,
          fraternidad: json.fraternidad,
      });
  }
}
