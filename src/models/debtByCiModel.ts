class DebtByCIModel {
    extraordinaria: {
      id: number;
      monto: number;
      concepto: string;
      create_date: string;
    };
    saldo: number;
    deuda: number;
  
    constructor(extraordinaria: any, saldo: number, deuda: number) {
      this.extraordinaria = extraordinaria;
      this.saldo = saldo;
      this.deuda = deuda;
    }
  
    static fromJson(debtByCIJson: any) {
      const { extraordinaria, saldo, deuda } = debtByCIJson;
      return new DebtByCIModel(extraordinaria, saldo, deuda);
    }
  }
  
  export default DebtByCIModel;
  