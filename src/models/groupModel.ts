class Group {
  id: number | string;
  nombre: string;
  dia: string;

  constructor({ id, nombre, dia }: { id: number | string; nombre: string; dia: string }) {
      this.id = id || '';
      this.nombre = nombre || '';
      this.dia = dia || '';
  }

  static fromJson(json: { id: number | string; nombre: string; dia: string }) {
      return new Group({
          id: json.id,
          nombre: json.nombre,
          dia: json.dia
      });
  }
}

export default Group;
