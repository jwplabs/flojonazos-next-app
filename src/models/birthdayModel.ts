export default class Birthday {
  user: string;
  disponible: boolean;
  fecha: Date;

  constructor({ user, disponible, fecha }: {
    user: string;
    disponible: boolean;
    fecha: string | number | Date;
  }) {
    this.user = user || '';
    this.disponible = disponible || false;
    this.fecha = new Date(fecha);
  }

  static fromJson(json: {
    user: string;
    disponible: boolean;
    fecha: string | number | Date;
  }) {
    return new Birthday({
      user: json.user,
      disponible: json.disponible,
      fecha: json.fecha,
    });
  }
}
