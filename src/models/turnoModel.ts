export default class Turno {
  id: number;
  fecha: string;
  grupo_turno: number;

  constructor({ id, fecha, grupo_turno }: { id: number; fecha: string; grupo_turno: number }) {
      this.id = id || 0;
      this.fecha = fecha || '';
      this.grupo_turno = grupo_turno || 0;
  }

  static fromJson(json: { id: number; fecha: string; grupo_turno: number }) {
      return new Turno({
          id: json.id,
          fecha: json.fecha,
          grupo_turno: json.grupo_turno,
      });
  }
}
