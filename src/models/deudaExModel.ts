export class Extraordinaria {
    id: number;
    monto: number;
    concepto: string;
    create_date: string;
  
    constructor(id: number, monto: number, concepto: string, create_date: string) {
      this.id = id;
      this.monto = monto;
      this.concepto = concepto;
      this.create_date = create_date;
    }
  
    static fromJson(extraordinariaJson: any) {
      const { id, monto, concepto, create_date } = extraordinariaJson;
      return new Extraordinaria(id, monto, concepto, create_date);
    }
  }
  
  
  class DeudaExtraordinariaModel {
    extraordinaria: Extraordinaria;
    saldo: number;
    deuda: number;
  
    constructor(extraordinaria: Extraordinaria, saldo: number, deuda: number) {
      this.extraordinaria = extraordinaria;
      this.saldo = saldo;
      this.deuda = deuda;
    }
  
    static fromJson(deudaJson: any) {
      const extraordinaria = Extraordinaria.fromJson(deudaJson.extraordinaria);
      const { saldo, deuda } = deudaJson;
      return new DeudaExtraordinariaModel(extraordinaria, saldo, deuda);
    }
  }
  
  export default DeudaExtraordinariaModel;
  