class Video {
    id: number;
    url: string;
    videoId: string;
    titulo: string;
    descripcion: string;
    mostrar: boolean;
    uploadDate: Date;
    fraternidad: number;
  
    constructor({
      id,
      url,
      videoId,
      titulo,
      descripcion,
      mostrar,
      uploadDate,
      fraternidad,
    }: {
      id: number;
      url: string;
      videoId: string;
      titulo: string;
      descripcion: string;
      mostrar: boolean;
      uploadDate: string;
      fraternidad: number;
    }) {
      this.id = id;
      this.url = url;
      this.videoId = videoId;
      this.titulo = titulo || '';
      this.descripcion = descripcion || '';
      this.mostrar = mostrar;
      this.uploadDate = new Date(uploadDate);
      this.fraternidad = fraternidad;
    }
  
    static fromJson(json: any): Video {
      return new Video({
        id: json.id,
        url: json.url,
        videoId: json.video_id,
        titulo: json.titulo,
        descripcion: json.descripcion,
        mostrar: json.mostrar,
        uploadDate: json.upload_date,
        fraternidad: json.fraternidad,
      });
    }
  }
  
  export default Video;
  