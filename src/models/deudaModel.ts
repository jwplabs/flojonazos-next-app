class DeudaModel {
    mes: string;
    gestion: number;
    costo: number;
    fecha: string;
    mensualidad: number;
  
    constructor(mes: string, gestion: number, costo: number, fecha: string, mensualidad: number) {
      this.mes = mes;
      this.gestion = gestion;
      this.costo = costo;
      this.fecha = fecha;
      this.mensualidad = mensualidad;
    }
  
    static fromJson(deudaJson: any) {
      const { mes, gestion, costo, fecha, mensualidad } = deudaJson;
      return new DeudaModel(mes, gestion, costo, fecha, mensualidad);
    }
  }
  
  export default DeudaModel;
  