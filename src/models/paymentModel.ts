class PaymentModel {
    id: number;
    mes: string;
    gestion: number;
    monto: number;
    fecha: string;
    created_date: string;
    pago: number;
    mensualidad: number;
  
    constructor(id: number, mes: string, gestion: number, monto: number, fecha: string, created_date: string, pago: number, mensualidad: number) {
        this.id = id;
        this.mes = mes;
        this.gestion = gestion;
        this.monto = monto;
        this.fecha = fecha;
        this.created_date = created_date;
        this.pago = pago;
        this.mensualidad = mensualidad;
    }
  
    static fromJson(paymentJson: any) {
        const { id, mes, gestion, monto, fecha, created_date, pago, mensualidad } = paymentJson;
        return new PaymentModel(id, mes, gestion, monto, fecha, created_date, pago, mensualidad);
    }
  }
  
  export default PaymentModel;
  