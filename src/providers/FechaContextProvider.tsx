// FechaContext.tsx
'use client'
import React, { createContext, useState, useContext } from 'react';


export interface FechaContextType {
  fechaSeleccionada: string;
  setFechaSeleccionada: React.Dispatch<React.SetStateAction<string>>;
}


const FechaContext = createContext<FechaContextType | undefined>(undefined);

export const FechaProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [fechaSeleccionada, setFechaSeleccionada] = useState<string>('');

  return (
    <FechaContext.Provider value={{ fechaSeleccionada, setFechaSeleccionada }}>
      {children}
    </FechaContext.Provider>
  );
};

export default FechaContext;
