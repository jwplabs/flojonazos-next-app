'use client'
import React, { createContext, useState, useContext } from 'react';


export interface QrContextType {
    fecha: string; // Nuevo campo fecha
    descripcion: string; // Nuevo campo descripcion 
    es_entresemana: boolean; // Nuevo campo es_entresemana
    tipo_evento: number; // Nuevo campo tipo_evento
    costo: string,
    url_qr: string,
    setFecha: React.Dispatch<React.SetStateAction<string>>;
    setDescripcion: React.Dispatch<React.SetStateAction<string>>;
    setEsEntresemana: React.Dispatch<React.SetStateAction<boolean>>;
    setTipoEvento: React.Dispatch<React.SetStateAction<number>>;
    setCosto: React.Dispatch<React.SetStateAction<string>>;
    setUrlQr: React.Dispatch<React.SetStateAction<string>>;
  }
  

const QrContext = createContext<QrContextType | undefined>(undefined);

export const QrProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
   
    const [fecha, setFecha] = useState<string>('');
    const [descripcion, setDescripcion] = useState<string>('');
    const [es_entresemana, setEsEntresemana] = useState<boolean>(false); // Cambiado el nombre para seguir convención de nombres
    const [tipo_evento, setTipoEvento] = useState<number>(0); // Cambiado el nombre para seguir convención de nombres
    const [costo, setCosto] = useState<string>('');
    const [url_qr, setUrlQr] = useState<string>('');
    return (
      <QrContext.Provider value={{
        fecha, setFecha,
        descripcion, setDescripcion,
        es_entresemana, setEsEntresemana, // Cambiado el nombre para seguir convención de nombres
        tipo_evento, setTipoEvento, // Cambiado el nombre para seguir convención de nombres
        costo, setCosto,
        url_qr, setUrlQr
      }}>
        {children}
      </QrContext.Provider>
    );
  };
  

export default QrContext;
