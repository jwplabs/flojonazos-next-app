import axios from 'axios';
import DeudaModel from '@/models/deudaModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchDeudasByMonth2() {
  const ci = localStorage.getItem('username');
  const END_POINT = `${API_URL}/api/agenda/deudas/m/${ci}`;
  try {

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    return response.data.deudas.map((deudaJson: any) => DeudaModel.fromJson(deudaJson));
  } catch (error) {
    console.error('Error fetching deudas:', error);
    throw error;
  }
}



export async function fetchDeudasByMonth() {
  const ci = localStorage.getItem('username');
  const END_POINT = `${API_URL}/api/agenda/deudas/m/${ci}`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    const deudas = response.data.deudas.map((deudaJson: any) => DeudaModel.fromJson(deudaJson));
    const total = response.data.total;

    return { deudas, total };  // Asegúrate de devolver un objeto con ambos campos
  } catch (error) {
    console.error('Error fetching deudas:', error);
    throw error;
  }
}

