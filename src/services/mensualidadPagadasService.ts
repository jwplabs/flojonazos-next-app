import axios from 'axios';
import PagoModel from '@/models/mensualidadPagadasModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchPagosByMonth(ci: string) {
  const END_POINT = `${API_URL}/api/agenda/pagos/m/${ci}`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    return response.data.map((pagoJson: any) => PagoModel.fromJson(pagoJson));
  } catch (error) {
    console.error('Error fetching pagos:', error);
    throw error;
  }
}
