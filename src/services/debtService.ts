import axios from 'axios';
import DebtModel from '@/models/debtModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchDebtsByMonth(ci: string) {
  const END_POINT = `${API_URL}api/agenda/deudas/m/${ci}`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    const { deudas, total } = response.data;
    const debts = deudas.map((debtJson: any) => DebtModel.fromJson(debtJson));
    return { debts, total };
  } catch (error) {
    console.error('Error fetching debts:', error);
    throw error;
  }
}
