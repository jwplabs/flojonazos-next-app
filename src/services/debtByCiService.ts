import axios from 'axios';
import DebtByCIModel from '@/models/debtByCiModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchDebtsByCI(ci: string) {
  const END_POINT = `${API_URL}api/agenda/deudas/e/${ci}`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    return DebtByCIModel.fromJson(response.data);
  } catch (error) {
    console.error('Error fetching debts by CI:', error);
    throw error;
  }
}
