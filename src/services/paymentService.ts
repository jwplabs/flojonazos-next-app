import axios from 'axios';
import PaymentModel from '@/models/paymentModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchPaymentsByMonth(ci: string) {
  const END_POINT = `${API_URL}/api/agenda/pagos/m/${ci}`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    return response.data.map((paymentJson: any) => PaymentModel.fromJson(paymentJson));
  } catch (error) {
    console.error('Error fetching payments:', error);
    throw error;
  }
}
