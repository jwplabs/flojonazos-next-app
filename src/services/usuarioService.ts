import axios from 'axios';
import User from '../models/usuarioModel';


const API_URL = process.env.NEXT_PUBLIC_API_URL;
export async function fetchUsers() {
    const END_POINT = `${API_URL}api/auth/users/`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    return response.data.map((userJson: any) => User.fromJson(userJson as {
      id: string;
      last_login: Date | null;
      username: string;
      ci: boolean;
      full_name: string;
      email: string;
      phone: string;
      financial_condition: string;
      role: string;
      copy_ci: boolean;
      avatar: boolean;
      suspend: boolean;
      is_staff: boolean;
      is_active: boolean;
      is_admin: boolean;
      is_superuser: boolean;
      verified: boolean;
      groups: any[];
      user_permissions: any[];
  }));
  } catch (error) {
    console.error('Error fetching users:', error);
    throw error;
  }
}

export async function fetchLastTesoreroEmail() {
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(`${API_URL}api/auth/users/`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    if (response.status === 200) {
      // Filtrar para obtener usuarios con el rol "Tesorero"
      const filteredUsers = response.data.filter((user:any) => user.role === 'Tesorero');

      if (filteredUsers.length > 0) {
        // Obtener el email del último tesorero
        const ultimoTesorero = filteredUsers[filteredUsers.length - 1];
        const emailTesorero = ultimoTesorero.email;
        return emailTesorero;
      } else {
        // Si no se encontraron Tesoreros, puedes devolver un valor por defecto o null
        return 'No se encontraron Tesoreros';
      }
    } else {
      // La solicitud fallo
      throw new Error(`Error al obtener datos: ${response.statusText}`);
    }
  } catch (error) {
    console.error('Error fetching tesorero email:', error);
    throw error;
  }
}
