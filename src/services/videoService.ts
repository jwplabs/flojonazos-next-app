import axios from 'axios';
import Video from '../models/videoModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchMediaVideos(): Promise<Video[]> {
  const END_POINT = `${API_URL}api/frater/mediavideos/`;

  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    return response.data.map((videoJson: any) => Video.fromJson(videoJson));
  } catch (error) {
    console.error('Error fetching media videos:', error);
    throw error;
  }
}


export async function fetchLastMediaVideo(): Promise<Video | null> {
  const END_POINT = `${API_URL}api/frater/mediavideos/`;

  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    const videos = response.data.map((videoJson: any) => Video.fromJson(videoJson));

    // Obtener el último video de la lista
    const lastVideo = videos.length > 0 ? videos[videos.length - 1] : null;

    return lastVideo;
  } catch (error) {
    console.error('Error fetching last media video:', error);
    throw error;
  }
}

