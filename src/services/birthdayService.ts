
import axios from 'axios';
import Birthday from '../models/birthdayModel';


const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchBirthdays() {
  const END_POINT = `${API_URL}api/frater/cumples`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
  
    return response.data.map((birthdayJson: any) => Birthday.fromJson(birthdayJson as {
      user: string;
      disponible: boolean;
      fecha: string | number | Date;
    }));
  } catch (error) {
    console.error('Error fetching birthdays:', error);
    throw error;
  }
}



export async function fetchBirthdaysDisponible() {
  const END_POINT = `${API_URL}api/frater/cumples`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    // Filtrar la lista de cumpleaños para devolver solo aquellos con disponible === true
    const birthdaysData = response.data.filter((birthday: any) => birthday.disponible === true);
    return birthdaysData.map((birthdayJson: any) => Birthday.fromJson(birthdayJson as  {
      user: string;
      disponible: boolean;
      fecha: string | number | Date;
    }));
  } catch (error) {
    console.error('Error fetching birthdays:', error);
    throw error;
  }
}
