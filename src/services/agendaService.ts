import axios from 'axios';
import Agenda from '../models/agendaModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchAgendasPendientes() {
    const END_POINT = `${API_URL}api/agenda/agendas/`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    if (response.status === 200) {
      
      const agendasPendientes = response.data.filter((agenda:any) => agenda.estado_reserva === 'pendiente');
      return agendasPendientes.map((agendaJson: any) => Agenda.fromJson(agendaJson as {
        id: number;
        fecha: string | number | Date;
        hora_inicio: string;
        hora_fin: string;
        descripcion: string;
        es_entresemana: boolean;
        estado_reserva: string;
        created_date: string | number | Date;
        tipo_evento: number;
        user: string;
      } ));
    } else {
      throw new Error('Failed to load agendas');
    }
  } catch (error) {
    console.error('Error fetching agendas:', error);
    throw error;
  }
}



export async function fetchAgendasConfirmadas() {
  const END_POINT = `${API_URL}api/agenda/agendas/`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

   
    // Realizar la solicitud HTTP
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // Comprobar si la solicitud fue exitosa (código de estado 200)
    if (response.status === 200) {
      // Filtrar la lista de agendas para obtener solo las confirmadas (estado_reserva = 'activa')
      const agendasConfirmadas = response.data.filter((agenda: any) => agenda.estado_reserva === 'activa');

      // Mapear los datos de agendas confirmadas al modelo Agenda y devolver la lista resultante
      return agendasConfirmadas.map((agendaData: any) => Agenda.fromJson(agendaData as {
        id: number;
        fecha: string | number | Date;
        hora_inicio: string;
        hora_fin: string;
        descripcion: string;
        es_entresemana: boolean;
        estado_reserva: string;
        created_date: string | number | Date;
        tipo_evento: number;
        user: string;
      }));
    } else {
      // Si la solicitud no fue exitosa, lanzar una excepción con un mensaje de error
      throw new Error('Failed to load agendas');
    }
  } catch (error) {
    // Capturar cualquier error que ocurra durante el proceso y lanzarlo nuevamente
    console.error('Error fetching agendas:', error);
    throw error;
  }
}


export async function createAgenda({
  fecha,
  horaInicio,
  horaFin,
  descripcion,
  tipoEvento,
  esEntreSemana, 
  estadoReserva,
  user
}: {
  fecha: string;
  horaInicio: string;
  horaFin: string;
  descripcion: string;
  tipoEvento: number;
  esEntreSemana: boolean; 
  estadoReserva: string;
  user: string;
}) {

  const accessToken = localStorage.getItem('accessToken');

  try {
    const response = await axios.post(`${API_URL}api/agenda/agendas/`, {
      fecha,
      hora_inicio: horaInicio,
      hora_fin: horaFin,
      descripcion,
      tipo_evento: tipoEvento,
      es_entresemana: esEntreSemana,
      estado_reserva: estadoReserva,
      user,
    }, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
      },
    });

    if (response.status === 201) {
      const responseData = response.data;
      return Agenda.fromJson(responseData);
    } else {
      throw new Error(`Failed to create agenda. Response: ${response.data}`);
    }
  } catch (error) {
    console.error('Error creating agenda:', error);
    throw error;
  }
}
