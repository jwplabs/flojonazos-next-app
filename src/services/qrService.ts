import axios from 'axios';
import QRModel from '@/models/qrModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchQRData() {
  const END_POINT = `${API_URL}api/agenda/qrs/`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    return response.data.map((qrJson: any) => QRModel.fromJson(qrJson));
  } catch (error) {
    console.error('Error fetching QR data:', error);
    throw error;
  }
}
export function findQRByValue(qrValue: string, qrData: QRModel[]) {
  const qrItem = qrData.find(qr => qr.qr_valor === qrValue);
  return qrItem ? qrItem.url : null;
}