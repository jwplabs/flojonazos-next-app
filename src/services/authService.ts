import axios from 'axios';


const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function login(username: string, password: string) {
  const END_POINT = `${API_URL}api/login`;
  
  try {
    const response = await axios.post(END_POINT, { username, password });
  
    if (!response.data) {
      throw new Error('Error de autenticación');
    }
  
    // Guardar tokens en el almacenamiento local
    localStorage.setItem('accessToken', response.data.access);
    localStorage.setItem('refreshToken', response.data.refresh);
    localStorage.setItem('lastRefreshDate', new Date().toISOString());
    

    
    const jwt = require('jsonwebtoken');

    // Supongamos que accessToken es tu token JWT
    const accessToken = response.data.access; // Aquí colocas tu token JWT

    try {
        const decodedToken = jwt.decode(accessToken);
        const { user_id, name, role, username, email } = decodedToken as any;

        // Guardar la información del usuario en el localStorage
        localStorage.setItem('user_id', user_id);
        localStorage.setItem('name', name);
        localStorage.setItem('role', role);
        localStorage.setItem('username', username);
        localStorage.setItem('email', email);

        // Mostrar la información del usuario en la consola
       /* console.log('user_id:', user_id);
        console.log('name:', name);
        console.log('role:', role);
        console.log('username:', username);
        console.log('email:', email);
     */
    } catch (error: any) {
        console.error('Error al decodificar el token:', error.message);
    }

    return response.data;
  } catch (error: any) {
    throw new Error('Error al iniciar sesión');
  }
}


export async function refreshTokenIfNeeded() {
  const lastRefreshDate = localStorage.getItem('lastRefreshDate');
  if (!lastRefreshDate) {
    throw new Error('No se encontró la fecha de la última actualización del token');
  }

  const currentDate = new Date();
  const lastRefresh = new Date(lastRefreshDate);
  const daysDifference = (currentDate.getTime() - lastRefresh.getTime()) / (1000 * 3600 * 24);

  if (daysDifference >= 6) {
    await refreshToken();
    localStorage.setItem('lastRefreshDate', new Date().toISOString());
  }
}

export async function refreshToken() {
  const refreshToken = localStorage.getItem('refreshToken');
  const accessToken = localStorage.getItem('accessToken');
  if (!refreshToken) {
    throw new Error('No se encontró el token de refresco en el almacenamiento local');
  }

  const END_POINT = `${API_URL}api/refresh`;

  try {
    const response = await axios.post(END_POINT, { refresh: refreshToken, access: accessToken });

    if (!response.data || !response.data.access) {
      throw new Error('Error al obtener el nuevo token de acceso');
    }
    
    // Actualizar el token de acceso en el almacenamiento local
    localStorage.setItem('accessToken', response.data.access);
    return response.data.access; // Devuelve el nuevo token de acceso
  } catch (error) {
    console.error('Error al refrescar el token de acceso:', error);
    throw new Error('Error al refrescar el token de acceso');
  }
}


/*
export async function refreshToken() {
  const refreshToken = localStorage.getItem('refreshToken');
  const accessToken = localStorage.getItem('accessToken');
  if (!refreshToken) {
    throw new Error('No se encontró el token de refresco en el almacenamiento local');
  }

  const END_POINT = `${API_URL}api/refresh`;

  try {
    const response = await axios.post(END_POINT, { refresh: refreshToken, access: accessToken });

    if (!response.data || !response.data.access) {
      throw new Error('Error al obtener el nuevo token de acceso');
    }
    
    //anterior
    console.log("aqui");
    console.log(accessToken);
    // Actualizar el token de acceso en el almacenamiento local
    localStorage.setItem('accessToken', response.data.access);
    console.log(accessToken);
    return response.data.access; // Devuelve el nuevo token de acceso
  } catch (error) {
    //localStorage.clear();
    console.error('Error al refrescar el token de acceso:', error);
    throw new Error('Error al refrescar el token de acceso');
   
  }
}
*/