import axios from 'axios';
import Turno from '../models/turnoModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchTurnos() {
    const END_POINT = `${API_URL}api/agenda/turnos/`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    return response.data.map((turnoJson: any) => Turno.fromJson(turnoJson as { id: number; fecha: string; grupo_turno: number }));

    //return response.data.map(turnoJson => Turno.fromJson(turnoJson));
  } catch (error) {
    console.error('Error fetching turnos:', error);
    throw error;
  }
}
