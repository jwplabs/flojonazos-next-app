import axios from 'axios';
import Group from '../models/groupModel';


const API_URL = process.env.NEXT_PUBLIC_API_URL;
export async function fetchGroups() {
    const END_POINT = `${API_URL}api/agenda/grupoturnos/`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    return response.data.map((groupJson: any) => Group.fromJson(groupJson as { id: number | string; nombre: string; dia: string }));
  } catch (error) {
    console.error('Error fetching groups:', error);
    throw error;
  }
}
