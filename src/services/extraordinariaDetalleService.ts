import axios from 'axios';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function getUserPayments() {
  try {
    const accessToken = localStorage.getItem('accessToken');
    const userId = localStorage.getItem('user_id');

    const paymentsResponse = await axios.get(`${API_URL}/api/agenda/pagos/`, {
      headers: {
        'Authorization': `Bearer ${accessToken}`,
      },
    });

    if (paymentsResponse.status === 200) {
      const paymentsData = paymentsResponse.data;

      // Filtrar los pagos del usuario dado
      const userPayments = paymentsData.filter((payment: any) => payment.user === userId);

      // Extraer las ID de los pagos del usuario dado
      const userPaymentIds = userPayments.map((payment : any) => payment.id);

      // Realizar una solicitud para obtener los pagos extraordinarios
      const extraResponse = await axios.get(`${API_URL}/api/agenda/pagoextraordinarias/`, {
        headers: {
          'Authorization': `Bearer ${accessToken}`,
        },
      });

      if (extraResponse.status === 200) {
        const extraPaymentsData = extraResponse.data;

        // Filtrar los pagos extraordinarios que coinciden con los pagos del usuario dado
        const userExtraPayments = extraPaymentsData.filter((extraPayment: any) => userPaymentIds.includes(extraPayment.pago));

        // Crear una lista final con la información necesaria de los pagos extraordinarios
        const finalPaymentsList = userExtraPayments.map((extraPayment: any) => {
          const matchingPayment = userPayments.find((payment: any) => payment.id === extraPayment.pago);
          return {
            id: matchingPayment.id,
            fecha_pago: matchingPayment.fecha_pago,
            monto_pagado: matchingPayment.monto_pagado,
            deuda_extraordinaria: extraPayment.extraordinaria,
          };
        });

        return finalPaymentsList;
      } else {
        throw new Error('Failed to load extraordinary payments data');
      }
    } else {
      throw new Error('Failed to load user payments data');
    }
  } catch (error) {
    console.error('Network error:', error);
    
  }
}
