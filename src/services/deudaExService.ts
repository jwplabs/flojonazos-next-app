import axios from 'axios';
import DeudaExtraordinariaModel from '@/models/deudaExModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchAllDeudaExtraordinaria() {
  const ci = localStorage.getItem('username');
  const END_POINT = `${API_URL}/api/agenda/deudas/e/${ci}`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    return response.data.map((deudaJson: any) => DeudaExtraordinariaModel.fromJson(deudaJson));
  } catch (error) {
    console.error('Error fetching deuda extraordinaria:', error);
    throw error;
  }
}

export async function fetchCurrentDeudaExtraordinaria() {
  try {
    const allDeudas = await fetchAllDeudaExtraordinaria();
    return allDeudas.find((deuda:any) => deuda.deuda > 0) || null;
  } catch (error) {
    console.error('Error fetching current deuda extraordinaria:', error);
    throw error;
  }
}
