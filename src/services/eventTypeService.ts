// services/EventTypeService.js

import axios from 'axios';
import EventType from '@/models/eventTypeModel';

const API_URL = process.env.NEXT_PUBLIC_API_URL;

export async function fetchEventTypes() {
  const END_POINT = `${API_URL}api/agenda/tiposevent/`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    return response.data.map((eventTypeJson: any) => EventType.fromJson(eventTypeJson as {
      id: number;
      nombre: string;
      descripcion: string;
      costo_entresemana: number;
      costo_finsemana: number;
  } ));
  } catch (error) {
    console.error('Error fetching event types:', error);
    throw error;
  }
}



export async function fetchEventTypePriceById(id: number, isWeekday: boolean) {
  const END_POINT = `${API_URL}api/agenda/tiposevent/${id}`;
  try {
    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });

    const eventTypeJson = response.data;
    
    // Obtener el precio correspondiente según el valor de isWeekday
    const precio = isWeekday ? eventTypeJson.costo_entresemana : eventTypeJson.costo_finsemana;

    return precio;
  } catch (error) {
    console.error('Error fetching event type price by id:', error);
    throw error;
  }
}