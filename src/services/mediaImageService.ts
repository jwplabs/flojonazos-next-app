import axios from 'axios';
import MediaImage from '../models/mediaImageModel';



const API_URL = process.env.NEXT_PUBLIC_API_URL;
export async function fetchMediaImages() {
    const END_POINT = `${API_URL}api/frater/mediaimages/`;
  try {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      throw new Error('Access token not found');
    }

    const response = await axios.get(END_POINT, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    return response.data.map((mediaImageJson: any) => MediaImage.fromJson(mediaImageJson as  { id: number; url: string; descripcion: string; secuencia: number; mostrar: boolean; upload_date: string | number | Date; fraternidad: number } ));


  } catch (error) {
    console.error('Error fetching media images:', error);
    throw error;
  }
}
